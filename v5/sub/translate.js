var fs = require('fs');
const path = require('path')
const puppeteer = require('puppeteer');
var myArgs = process.argv.slice(2);
// fs.readFile('silence-2006/Silence.E01.srt', 'utf8', function(err, contents) {
//     // console.log(contents);
//     console.log(1);
// });
function strip(s) {
    return s.replace(/^\s+|\s+$/g,"");
}
var filename = myArgs[0];
// var newfilename = filename.replace(".srt","")+"-id.srt";
var newfilename = filename.split("/");
newfilename[newfilename.length] = newfilename[newfilename.length-1];
newfilename[newfilename.length-2] = "id";
newfilename = newfilename.join("/");
//
// console.log(newfilename);
// throw "er";

var srt = fs.readFileSync(filename, 'utf8');
var textonly = "",
    tmptext="";
var i2 = 0;
var text = [],
    translated = [];
srt = srt.replace(/\r\n|\r|\n/g, '\n');
srt = strip(srt);
srt = srt.split('\n\n');

var srtArr = [];
var splited,
    row="";
for(var k in srt){
    splited = srt[k].split('\n');
    var val = "",td="";
    var row = new Object();
    for(var x in splited){
        val += splited[x];

        if(x==0){
            // td += `<td>${val}</td>`;
            row.id = val;
            val="";
        }else if(x==1){
            row.time = val;
            val="";
        }else{
            val +=" ; ";
        }
        if(x == splited.length-1){
            row.text = val;
            tmptext = val+" ;; ";
            if(textonly.length+tmptext.length>5000){
                textonly = "";
                i2++;
            }
            textonly += tmptext+"\n";
            if(typeof text[i2]==='undefined')text[i2]=[];
            text[i2].push([tmptext]);
        }
    }
    srtArr.push(row);
    // if(k>10)break;
}

// break;
for(var k in text){
    var joinedText = text[k].join("");
    // console.log(joinedText.length);break;
    text[k] = joinedText;
    break;
}
// console.log(text);throw "er";
translated = doTranslate(text);

function parseResult(translated) {
    console.log(`parsing translated text`);
    translated = translated.substr(0,translated.length-4);
    var trArr = translated.split(";;");

    for(var k in trArr){
        var z =  trArr[k].split(";"),
            newtext="";

        for(x in z){
            if(z[x].trim().substr(0,2)==",,"){
                z[x] = z[x].trim().substr(2);
            }else if(z[x].trim().substr(0,1)==","){
                z[x] = z[x].trim().substr(1);
            }
            newtext+=z[x].trim();
            if(x<z.length-1)newtext+="\n";
        }
        srtArr[k].text = newtext;
    }
    parseArrToSrt(srtArr);
}

function parseArrToSrt(arr) {
    var result="";
    for(var k in arr){
        var row = arr[k];
        for(var z in row){
            result += row[z]+"\n";
        }
    }
    // console.log(result);
    // writeFileSyncRecursive(newfilename,result);
    fs.writeFile(newfilename, result, function (err) {
        if (err) throw err;
        console.log('Saved!');
    });
}
function writeFileSyncRecursive(filename, content) {
    const folders = filename.split(path.sep).slice(0, -1)
    if (folders.length) {
        // create folder path if it doesn't exist
        folders.reduce((last, folder) => {
            const folderPath = last ? last + path.sep + folder : folder
            if (!fs.existsSync(folderPath)) {
            fs.mkdirSync(folderPath)
        }
        return folderPath
    })
    }

}

async function doTranslate(w) {
    let launchOptions = { headless: true, args: ['--start-maximized'] };
    console.log("opening browser..");
    const browser = await puppeteer.launch(launchOptions);
    const page = await browser.newPage();

// set viewport and user agent (just in case for nice viewing)
    await page.setViewport({width: 1366, height: 768});
    await page.setUserAgent('Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.108 Safari/537.36');

// define source and target language code
    let sourceLang = 'auto', targetLang = 'id';
    console.log("accessing google translate..");
    await page.goto(`https://translate.google.com/#view=home&op=translate&sl=${sourceLang}&tl=${targetLang}`);

// detect the source textarea for input data (source string)
//     await page.waitForSelector('#source');
//     await page.waitFor(1000);

// string that we want to translate and type it on the textarea
    let sourceString = w;
    // await page.type('#source', sourceString);

    var translatedResult="";
    for (var k in w){
        console.log(`translating text ${k}`);
        // await page.$eval('textarea', textarea => textarea.value = w);
        var str = w[k];
        await page.evaluate((str) => {
                document.querySelector('#source').value = str;
        }, str);


// wait for the result container available
        await page.waitForSelector('.result-shield-container');
        await page.waitFor(3000);

// get the result string (translated text)
        translatedResult += await page.evaluate(() => {
                return document.querySelectorAll('.result-shield-container')[0].textContent;
        });
    }


// display the source and translated text to console
//     console.log(`${sourceLang}: ${sourceString}\n${targetLang}: ${translatedResult}`);

    await page.waitFor(1000);
    await browser.close();
    parseResult(translatedResult);

}
// (async () => {
//
// })();