<?php

define('SRT_STATE_SUBNUMBER', 0);
define('SRT_STATE_TIME',      1);
define('SRT_STATE_TEXT',      2);
define('SRT_STATE_BLANK',     3);

$filename = $argv[1];
$lines   = file($filename);

$subs    = array();
$state   = SRT_STATE_SUBNUMBER;
$subNum  = 0;
$subText = '';
$subTime = '';
$textonly = "";
$i2 = 0;
$text = array();
$idx= 0;
$subResult="";
foreach($lines as $line) {
    switch($state) {
        case SRT_STATE_SUBNUMBER:
            $subNum = trim($line);
            $state  = SRT_STATE_TIME;
            break;

        case SRT_STATE_TIME:
            $subTime = trim($line);
            $state   = SRT_STATE_TEXT;
            break;

        case SRT_STATE_TEXT:
            if (trim($line) == '') {
                $sub = new stdClass;
                $sub->number = $subNum;
                list($sub->startTime, $sub->stopTime) = explode(' --> ', $subTime);
                $sub->text   = $subText;
                $tmptext = $subText."[##]";
                if(strlen($textonly.$tmptext.PHP_EOL)>5000){
                    $textonly = "";
                    $i2++;
                }
                $textonly .= $tmptext.PHP_EOL;
                $text[$i2][] = $tmptext;
                $subText     = '';
                $state       = SRT_STATE_SUBNUMBER;

                $subs[]      = $sub;
                $idx++;
            } else {
                $subText .= $line;
            }
            break;
    }
//    if(count($subs)>100)break;
}

foreach ($text as $k => $t){
    $res = implode("",$t);
//    $res = substr($res,0,-4);
    file_put_contents("translated/a$k.txt",$res);
}

if ($state == SRT_STATE_TEXT) {
    // if file was missing the trailing newlines, we'll be in this
    // state here.  Append the last read text and add the last sub.
    $sub->text = $subText;
    $subs[] = $sub;

}