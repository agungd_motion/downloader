<?php

function getdetailDQ($movieurl,$season){
    $result = array();
    $itemsframe = array();
    $iframeurl = '';
    $htmllink = file_get_html($movieurl,0);
    //var_dump($htmllink);
    $toupdate = array();
    $episodes = array();
    if($htmllink===false){
        $htmllink = file_get_html($movieurl,0);
    }
    if($htmllink===false){
        $htmllink = file_get_html($movieurl,0);
    }
    if($htmllink===false){
        return $toupdate;
    }
    
    $html2 = $htmllink->innertext;
    $kode = get_string_between($html2, 'document.write( unescape( "%', '" ) );');
    if (!empty($kode)) $kode = "%" . $kode;
    $kodescript = htmlspecialchars(urldecode($kode));
    $src = get_string_between(htmlspecialchars_decode($kodescript), '<iframe src="', '"');

    $toupdate['iframeurl'] = $src;

    foreach ($htmllink->find("div#action-parts span") as $k => $e){
        $row = array();
        $rawlabel = trim($e->innertext);
        $label = trim(str_replace("eps","",strtolower($rawlabel)));
        $numeps = trim(str_replace("end","",strtolower($label)));

        $numeps = explode("-",$numeps)[0];
        $numeps = explode("*",$numeps)[0];
        $numeps = explode(" ",$numeps)[0];

        if(strpos($label,"-")!==false){
            $numeps = $k+1;
        }
        if(!is_numeric($numeps)){
            $numeps = $k+1;
        }
        $numeps = intval($numeps);
//        $numeps = $k+1;
        if($k==0){
            $epsurl = $movieurl;
        }else{
            $epsurl = $e->parent()->href;
        }

        $row = array(
            "label" => $rawlabel,
            "numeps" => $numeps,
            "url" => $epsurl,
            "season" => $season
        );
        array_push($episodes,$row);
    }
    if($season>0 and count($episodes)==0){
        $row = array(
            "label" => "1",
            "numeps" => 1,
            "url" => $movieurl,
            "season" => $season
        );
        array_push($episodes,$row);
    }
    $toupdate['episodes'] = $episodes;

    $source=array();

    return $result = array_merge($toupdate,$source);

    //print_r($result);


}

function getSourceDQ($iframeurl)
{
    $html2 = file_get_contents($iframeurl);

    $id = get_string_between($html2,'Player_Load("', '","');
    $sub = get_string_between($html2,'","', '");');
    $loaderapi = "https://drmq.stream/v4/loader.php";
    $post = "id=$id";
    $res = getContent($loaderapi,"POST",$post);
    $vv = array();
    if(!empty($res)){
        $rd = json_decode(trim($res),true);
        foreach ($rd as $k => $v){
            array_push($vv, $v);
        }
    }

    $toupdate["videourl"] = $vv;//htmlspecialchars_decode($decodedparsed);
    $toupdate["subtitleurl"] = $sub;

    return $toupdate;
}
