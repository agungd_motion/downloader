function get_between(str, first, last) {
    return str.substring(str.lastIndexOf(first) + first.length, str.lastIndexOf(last));
}
function onGetUrlResponse(url) {
    var purl = url.split("?sub=");
    url = purl[0];
    if (purl.length > 1) {
        if(purl[1]!=''){
            subtitles.push({
                url: purl[1],
                name: "id"
            })
        }
    }
    if(url.indexOf("_dev.")>=0){
        url = url.replace("_dev","");
    }
    var rpath = url.split("/");
    baseurl=rpath[0]+"//"+rpath[2];
    var newUrl = url;
    urlMain = newUrl;
    urlReferrer = url;
    httpClient.postMessage(JSON.stringify({
        id: "SK_MAIN",
        url:newUrl,
    }));
    return 1;
}
function onHttpClientResponse(id, content) {
    switch(id) {
        case "SK_MAIN":
            decode = window.atob(content);
            var el = document.createElement('html');
            el.innerHTML = decode;
            var  mp = el.getElementsByClassName("movieplay");
            if(mp.length>0){
                var iframe = mp[0].getElementsByTagName("iframe");
                if(iframe.length>0){

                    var url = iframe[0].getAttribute("src");
                    httpClient.postMessage(JSON.stringify({
                        id: "SK_IFRAME",
                        url: url,
                    }));
                }
            }
            break;
        case "SK_IFRAME":
            decode = window.atob(content);
            var clean = decode.replace(/\s+/g, "");
            var vv = get_between(clean,'playerInstance.setup({sources:',',tracks:');
            var ss = get_between(clean,',tracks:',',skin:');
            vv = eval(vv);
            ss = eval(ss);

            var videos = [];
            for (var i = 0; i < vv.length; i++) {
                var item = {
                    url: vv[i]['file'],
                    name: vv[i]['label']
                };
                if(vv[i]['label'].includes("360"))videos.unshift(item);
                else videos.push(item);
            }

            for (var i = 0; i < ss.length; i++) {
                var surl = ss[i]['file'];
                if(surl.indexOf("http")<0)surl = baseurl + surl;
                var item = {
                    url: surl,
                    name: ss[i]['label']
                };
                subtitles.push(item);
            }

            var result = {
                videos: videos,
                subtitles: subtitles
            }
            setVideoInfo.postMessage(JSON.stringify(result));
            break;
        case "FEMBED":
            decode = Base64.decode(content);
            var j = JSON.parse(decode);
            var vv = j.data;
            var videos = [];
            for (var i = 0; i < vv.length; i++) {
                var item = {
                    url: vv[i]['file'],
                    name: vv[i]['label']
                };
                videos.push(item);
            }
            var result = {
                videos: videos,
                subtitles: subtitles
            };
            setVideoInfo.postMessage(JSON.stringify(result));
            break;
        case "GDRIVEPLAYER":
            var c = Base64.decode(content);
            var pass = "alsfheafsjklNIWORNiolNIOWNKLNXakjsfwnBdwjbwfkjbJjkopfjweopjASoiwnrflakefneiofrt";
            var CryptoJSAesJson = {
                stringify: function (cipherParams) {
                    var j = {ct: cipherParams.ciphertext.toString(CryptoJS.enc.Base64)};
                    if (cipherParams.iv) j.iv = cipherParams.iv.toString();
                    if (cipherParams.salt) j.s = cipherParams.salt.toString();
                    return JSON.stringify(j);
                },
                parse: function (jsonStr) {
                    var j = JSON.parse(jsonStr);
                    var cipherParams = CryptoJS.lib.CipherParams.create({ciphertext: CryptoJS.enc.Base64.parse(j.ct)});
                    if (j.iv) cipherParams.iv = CryptoJS.enc.Hex.parse(j.iv)
                    if (j.s) cipherParams.salt = CryptoJS.enc.Hex.parse(j.s)
                    return cipherParams;
                }
            }

            var script = get_between(c,"eval(function","{}))");
            script = "eval(function" + script + "{}))";
            var unpack = P_A_C_K_E_R.unpack(script);
            var data = unpack.split('var data=').pop().split(';')[0];
            eval("var data="+data+";");
            var dec = JSON.parse(CryptoJS.AES.decrypt(data, pass, {format: CryptoJSAesJson}).toString(CryptoJS.enc.Utf8));
            var res = P_A_C_K_E_R.unpack(dec);
            var vv = eval(res.split('player.setup({sources:').pop().split(',image:')[0]);
            var videos = [];
            for (var i = 0; i < vv.length; i++) {
                var item = {
                    url: vv[i]['file'],
                    name: vv[i]['label']
                };
                if(vv[i]['label'].includes("360"))videos.unshift(item);
                else videos.push(item);
            }

            var result = {
                videos: videos,
                subtitles: subtitles
            };
            setVideoInfo.postMessage(JSON.stringify(result));
            break;
    }
    return 1;
}
function getFembed(url) {
    var t = url.split("/"),
        r = t[t.length - 1];
    newUrl = "https://feurl.com/api/source/" + r;
    var n = JSON.stringify({
        id: "FEMBED",
        url: newUrl,
        method: "POST"
    });
    httpClient.postMessage(n)
}

var delayCloudFlare = 0,
    urlIframe = "",
    baseurl = "",
    urlOne = "",
    urlTwo = "",
    decode = "",
    urlReferrer = "",
    urlLoader = "",
    sub = "",
    log = {},
    subtitles = [],
    iframeurl = [];

function main(){
    getUrl.postMessage("");
    return 1;
}
main();