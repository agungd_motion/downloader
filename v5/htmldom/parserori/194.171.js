var P_A_C_K_E_R={detect:function(e){return P_A_C_K_E_R.get_chunks(e).length>0},get_chunks:function(e){var c=e.match(/eval\(\(?function\(.*?(,0,\{\}\)\)|split\('\|'\)\)\))($|\n)/g);return c||[]},unpack:function(e){for(var c,n=P_A_C_K_E_R.get_chunks(e),t=0;t<n.length;t++)c=n[t].replace(/\n$/,""),e=e.split(c).join(P_A_C_K_E_R.unpack_chunk(c));return e},unpack_chunk:function(e){var c="",n=eval;if(P_A_C_K_E_R.detect(e))try{eval=function(e){return c+=e},n(e),"string"==typeof c&&c&&(e=c)}catch(e){}return eval=n,e},run_tests:function(e){var c=e||new SanityTest,n="eval(function(p,a,c,k,e,r){e=String;if(!''.replace(/^/,String)){while(c--)r[c]=k[c]||c;k=[function(e){return r[e]}];e=function(){return'\\\\w+'};c=1};while(c--)if(k[c])p=p.replace(new RegExp('\\\\b'+e(c)+'\\\\b','g'),k[c]);return p}('0 2=1',3,3,'var||a'.split('|'),0,{}))",t="eval(function(p,a,c,k,e,r){e=String;if(!''.replace(/^/,String)){while(c--)r[c]=k[c]||c;k=[function(e){return r[e]}];e=function(){return'\\\\w+'};c=1};while(c--)if(k[c])p=p.replace(new RegExp('\\\\b'+e(c)+'\\\\b','g'),k[c]);return p}('0 2=1',3,3,'foo||b'.split('|'),0,{}))",r="eval(function(p,a,c,k,e,r){BORKBORK;if(!''.replace(/^/,String)){while(c--)r[c]=k[c]||c;k=[function(e){return r[e]}];e=function(){return'\\\\w+'};c=1};while(c--)if(k[c])p=p.replace(new RegExp('\\\\b'+e(c)+'\\\\b','g'),k[c]);return p}('0 2=1',3,3,'var||a'.split('|'),0,{}))";c.test_function(P_A_C_K_E_R.detect,"P_A_C_K_E_R.detect"),c.expect("",!1),c.expect("var a = b",!1),c.test_function(P_A_C_K_E_R.unpack,"P_A_C_K_E_R.unpack"),c.expect(r,r),c.expect(n,"var a=1"),c.expect(t,"foo b=1"),c.expect("eval(function(p,a,c,k,e,r){e=String;if(!''.replace(/^/,String)){while(c--)r[c]=k[c]||c;k=[function(e){return r[e]}];e=function(){return'\\\\w+'};c=1};while(c--)if(k[c])p=p.replace(new RegExp('\\\\b'+e(c)+'\\\\b','g'),k[c]);return p}('0 2=1{}))',3,3,'var||a'.split('|'),0,{}))","var a=1{}))");var i="\nfiller\n";return c.expect(i+n+"\n"+r+i+t+i,i+"var a=1\n"+r+i+"foo b=1"+i),c}};
function get_between(str, first, last) {
    return str.substring(str.lastIndexOf(first) + first.length, str.lastIndexOf(last));
}
function onGetUrlResponse(url) {
    var purl = url.split("?sub=");
    url = purl[0];
    if (purl.length > 1) {
        if(purl[1]!=''){
            subtitles.push({
                url: purl[1],
                name: "id"
            })
        }
    }
    if(url.indexOf("_dev.")>=0){
        url = url.replace("_dev","");
    }
    if(url.indexOf("103.194.171.205")>=0){
        var id = "IFMAIN";
    }else if(url.indexOf("103.194.171.18")>=0){
        var id = "FAMAIN";
    }
   
    var newUrl = url;
    urlMain = newUrl;
    urlReferrer = url;
    httpClient.postMessage(JSON.stringify({
        url: newUrl,
        id:id
    }));
    return 1;
}
function onHttpClientResponse(id, content) {
    switch(id) {
        case "IFMAIN":
            decode = window.atob(content);
            var el = document.createElement('html');
            var postid,cl,tabs,lis,playerid;
            el.innerHTML = decode;
            var movieplay = el.getElementsByTagName('iframe');
            if(movieplay.length>0){
                iframeurl = movieplay[0].getAttribute("src");
                var headers= {
                    "referer": urlReferrer
                };
                httpClient.postMessage(JSON.stringify({
                    id: "IFRAME",
                    url: iframeurl,
                    header:headers
                }));
            }
            break;
        case "IFRAME":
            decode = window.atob(content);
            var script = get_between(decode,"eval(function","{}))");
            script = "eval(function" + script + "{}))";

            var res = P_A_C_K_E_R.unpack(script);
            var vv = res.split('sources:').pop().split('}],')[0];
            vv = get_between(vv,"[","]")
            var vv = eval("["+vv+"]");
            var videos = [];
            for (var i = 0; i < vv.length; i++) {
                var label = vv[i]['label'] || "default";
                var item = {
                    url: vv[i]['file'],
                    name: label
                };
                if (label.includes("360")) {
                    videos.unshift(item)
                } else {
                    videos.push(item)
                }
                videos.push(item);
            }
            var result = {
                videos: videos,
                subtitles: subtitles
            };
            setVideoInfo.postMessage(JSON.stringify(result));
            break;
        case "FAMAIN":
            var decode = window.atob(content);
            var clean = decode.replace(/\s+/g, "");
            urlIframe = get_between(clean, '<iframeid="myFrame"src="', '"width="100%"');
            if(urlIframe.includes('stream5')!==true){
                var del = get_between(urlIframe,'/','.php');
                urlIframe = urlIframe.replace(del,'stream5');
            }

            var el = document.createElement('html');
            el.innerHTML = decode;
            var x = el.getElementsByClassName('les-content');
            var x1 = x[0].getElementsByTagName('a');
            if(x1.length>1){
                url1 = x1[1].getAttribute('href');
            }
            httpClient.postMessage(JSON.stringify({
                url: urlIframe,
                id:"FAIFRAME"
            }));
            break;
        case "FAIFRAME":
            var decode = window.atob(content);

            var clean = decode.replace(/\s+/g, "");
            var video="",label;

            if(clean.indexOf('<iframesrc="')>=0){
                var iurl = get_between(clean, '<iframesrc="', '"width="100%');
                if(iurl.indexOf('https://drive.google.com/file/d/')>=0){
                    var id = get_between(iurl, 'https://drive.google.com/file/d/', '/preview');
                    var cek = 'https://www.googleapis.com/drive/v3/files/'+id+'?alt=json&key=AIzaSyBkK04Xe0ZzIRSx1TcZyHvkkTGEtkPgugw';
                    // var gv = 'https://docs.google.com/get_video_info?docid='+id;
                    // dlurl = 'https://drive.google.com/uc?export=download&id='+id;
                    // var gdriveplayer = "https://gdriveplayer.co/embed2.php?link="+iurl;

                    httpClient.postMessage(JSON.stringify({
                        id:"cek",
                        url: cek,
                    }));
                    label = "google";
                }
            }else if(clean.includes('sourcesAPI')!==false){
                var x = clean.split("varsourcesAPI=[").pop().split("];")[0];
                video = get_between(x,"'file':'","'?e=download'}],");
                label = get_between(x,"'label':'","',");
            }else{
                var x = get_between(clean,"jwplayer('my_video').setup({",",});");
                video = get_between(x,'"file":"','?e=download"}],');
                label = get_between(x,'sources:[{"label":"','",');
            }
 
            if(video!='') {
                // if(video.includes('http')!==true){
                //     if(url1 == url2){
                //         return null;
                //     }
                //     url2 = url1;
                //     onGetUrlResponse(url1);
                // }else{
                //     var videos = [];
                //     var item = {
                //         url: video,
                //         name: label
                //     };
                //     videos.push(item);
                //     var result = {
                //         videos: videos,
                //         subtitles: subtitles
                //     };
                //     setVideoInfo.postMessage(JSON.stringify(result));
                //
                // }
            }
            break;
        case 'cek':
            var decode = window.atob(content);
            var json = JSON.parse(decode);
            var videos = [];
            var result = {
                videos: videos,
                subtitles: subtitles
            };
            if(json.mimeType=='video/mp4'){
                var item = {
                    url: 'https://www.googleapis.com/drive/v3/files/'+json.id+'?alt=media&key=AIzaSyBkK04Xe0ZzIRSx1TcZyHvkkTGEtkPgugw',
                    name: 'google'
                };
                videos.push(item);
                result = {
                    videos: videos,
                    subtitles: subtitles
                };
                setVideoInfo.postMessage(JSON.stringify(result));
            }else{
                if(url1!=''){
                    if(url1 == url2){
                        setVideoInfo.postMessage(JSON.stringify(result));
                    }
                    url2 = url1;
                    onGetUrlResponse(url1);
                }else {
                    fetch('http://dev.motion.co.id/robo/v4/HLS/log.php', {method: 'post',
                        body: JSON.stringify({a: "else"})
                    });
                    setVideoInfo.postMessage(JSON.stringify(result));
                }
            }

            break;
    }
    return 1;
}
var delayCloudFlare = 0,
    urlMain = "",
    decode = "",
    urlIframe = "",
    urlOne = "",
    urlTwo = "",
    urlReferrer = "",
    sub = "",
    subtitles = [],
    videos=[],
    log={},
    iframeurl = [],
    url1='',
    url2='';

function main(){
    getUrl.postMessage("");
    return 1;
}
main();