var P_A_C_K_E_R={detect:function(e){return P_A_C_K_E_R.get_chunks(e).length>0},get_chunks:function(e){var c=e.match(/eval\(\(?function\(.*?(,0,\{\}\)\)|split\('\|'\)\)\))($|\n)/g);return c||[]},unpack:function(e){for(var c,n=P_A_C_K_E_R.get_chunks(e),t=0;t<n.length;t++)c=n[t].replace(/\n$/,""),e=e.split(c).join(P_A_C_K_E_R.unpack_chunk(c));return e},unpack_chunk:function(e){var c="",n=eval;if(P_A_C_K_E_R.detect(e))try{eval=function(e){return c+=e},n(e),"string"==typeof c&&c&&(e=c)}catch(e){}return eval=n,e},run_tests:function(e){var c=e||new SanityTest,n="eval(function(p,a,c,k,e,r){e=String;if(!''.replace(/^/,String)){while(c--)r[c]=k[c]||c;k=[function(e){return r[e]}];e=function(){return'\\\\w+'};c=1};while(c--)if(k[c])p=p.replace(new RegExp('\\\\b'+e(c)+'\\\\b','g'),k[c]);return p}('0 2=1',3,3,'var||a'.split('|'),0,{}))",t="eval(function(p,a,c,k,e,r){e=String;if(!''.replace(/^/,String)){while(c--)r[c]=k[c]||c;k=[function(e){return r[e]}];e=function(){return'\\\\w+'};c=1};while(c--)if(k[c])p=p.replace(new RegExp('\\\\b'+e(c)+'\\\\b','g'),k[c]);return p}('0 2=1',3,3,'foo||b'.split('|'),0,{}))",r="eval(function(p,a,c,k,e,r){BORKBORK;if(!''.replace(/^/,String)){while(c--)r[c]=k[c]||c;k=[function(e){return r[e]}];e=function(){return'\\\\w+'};c=1};while(c--)if(k[c])p=p.replace(new RegExp('\\\\b'+e(c)+'\\\\b','g'),k[c]);return p}('0 2=1',3,3,'var||a'.split('|'),0,{}))";c.test_function(P_A_C_K_E_R.detect,"P_A_C_K_E_R.detect"),c.expect("",!1),c.expect("var a = b",!1),c.test_function(P_A_C_K_E_R.unpack,"P_A_C_K_E_R.unpack"),c.expect(r,r),c.expect(n,"var a=1"),c.expect(t,"foo b=1"),c.expect("eval(function(p,a,c,k,e,r){e=String;if(!''.replace(/^/,String)){while(c--)r[c]=k[c]||c;k=[function(e){return r[e]}];e=function(){return'\\\\w+'};c=1};while(c--)if(k[c])p=p.replace(new RegExp('\\\\b'+e(c)+'\\\\b','g'),k[c]);return p}('0 2=1{}))',3,3,'var||a'.split('|'),0,{}))","var a=1{}))");var i="\nfiller\n";return c.expect(i+n+"\n"+r+i+t+i,i+"var a=1\n"+r+i+"foo b=1"+i),c}};

function get_between(str, left, right) {
    leftpos = str.lastIndexOf(left) + left.length;
    rightpos = str.indexOf(right, leftpos);
    return str.substring(leftpos, rightpos);
}


function onGetUrlResponse(url) {
    // url = "https://filmapik.de/episodes/touch-1x3";
    // url = "https://filmapik.fun/episodes/woman-of-9-9-billion-1x1-2";

    // url = "https://filmapik.de/episodes/black-dog-1x1";
    // url = "http://103.194.171.18/episodes/forest-1x11-12";
    if(url.indexOf("///")>=0){
        url = url.replace("///","/");
    }
    if(url.includes('/episodes')== false && url.includes('/play')==false){
        url = url+'/play';
    }
    urlMain = url;


    httpClient.postMessage(JSON.stringify({
        url: urlMain
    }));

    return 1;
}

function onHttpClientResponse(id, content) {
    switch (id) {
        case urlMain:
            var decode = window.atob(content);
            var clean = decode.replace(/\s+/g, "");
            urlIframe = get_between(clean, '<iframeid="myFrame"src="', '"width="100%"');
            if(urlIframe.includes('stream5')!==true){
                var del = get_between(urlIframe,'/','.php');
                urlIframe = urlIframe.replace(del,'stream5');
            }

            var el = document.createElement('html');
            el.innerHTML = decode;
            var x = el.getElementsByClassName('les-content');
            var x1 = x[0].getElementsByTagName('a');
            if(x1.length>1){
                url1 = x1[1].getAttribute('href');
            }
            httpClient.postMessage(JSON.stringify({
                url: urlIframe
            }));
            break;

        case urlIframe:
            var decode = window.atob(content);
            var clean = decode.replace(/\s+/g, "");
            var video="",label;

            if(clean.indexOf('<iframesrc="')>=0){
                var iurl = get_between(clean, '<iframesrc="', '"width="100%');
                if(iurl.indexOf('https://drive.google.com/file/d/')>=0){
                    var id = get_between(iurl, 'https://drive.google.com/file/d/', '/preview');
                    // video = `https://www.googleapis.com/drive/v3/files/${id}?alt=media&key=AIzaSyBkK04Xe0ZzIRSx1TcZyHvkkTGEtkPgugw`;
                    var gv = 'https://docs.google.com/get_video_info?docid='+id;
                    dlurl = 'https://drive.google.com/uc?export=download&id='+id;
                    var gdriveplayer = "https://gdriveplayer.co/embed2.php?link="+iurl;

                    // httpClient.postMessage(JSON.stringify({
                    //     id:"GET_VIDEO_INFO",
                    //     url: gv,
                    // }));
                    htmlRequest.postMessage(JSON.stringify({
                        url:dlurl,
                    }));
                    label = "google";
                }
            }else if(clean.includes('sourcesAPI')!==false){
                var x = clean.split("varsourcesAPI=[").pop().split("];")[0];
                video = get_between(x,"'file':'","'?e=download'}],");
                label = get_between(x,"'label':'","',");
            }else{
                var x = get_between(clean,"jwplayer('my_video').setup({",",});");
                video = get_between(x,'"file":"','?e=download"}],');
                label = get_between(x,'sources:[{"label":"','",');
            }

            if(video!='') {
                if(video.includes('http')!==true){
                    if(url1 == url2){
                        return null;
                    }
                    url2 = url1
                    onGetUrlResponse(url1);
                }else{
                    var item = {
                        url: video,
                        name: label
                    };
                    videos.push(item);

                    var result = {
                        videos: videos,
                        subtitles: subtitles
                    };
                    setVideoInfo.postMessage(JSON.stringify(result));
                }
            }

            break;
        case 'dlpage':
            var c = window.atob(content);
            var el = document.createElement('html');
            el.innerHTML = c;
            var x = el.getElementsByClassName('jfk-button-action');
            if(x.length>0){
                var confirm = x[0].getAttribute("href");
                if(confirm.substring(0,1)=="/"){
                    confirm = "https://drive.google.com"+confirm;
                }
                var key = get_between(confirm,"confirm=","&id");
                var id = confirm.split("id=").pop();
                var header = {
                    'Cookie':'download_warning_03032010689956712104_'+id+'='+key
                };

                console.log(header);
                console.log(confirm);
                httpClient.postMessage(JSON.stringify({
                    id:"dlconfirm",
                    url: confirm,
                    header:header
                }));
            }
            break;
        case 'dlconfirm':
            var c = window.atob(content);
            fetch('http://dev.motion.co.id/robo/v4/HLS/log.php', {method: 'post',
                body:c
            });
            console.dir(c);
            break;
        case "GDRIVEPLAYER":
            var c = window.atob(content);
            var pass = "alsfheafsjklNIWORNiolNIOWNKLNXakjsfwnBdwjbwfkjbJjkopfjweopjASoiwnrflakefneiofrt";
            var CryptoJSAesJson = {
                stringify: function (cipherParams) {
                    var j = {ct: cipherParams.ciphertext.toString(CryptoJS.enc.Base64)};
                    if (cipherParams.iv) j.iv = cipherParams.iv.toString();
                    if (cipherParams.salt) j.s = cipherParams.salt.toString();
                    return JSON.stringify(j);
                },
                parse: function (jsonStr) {
                    var j = JSON.parse(jsonStr);
                    var cipherParams = CryptoJS.lib.CipherParams.create({ciphertext: CryptoJS.enc.Base64.parse(j.ct)});
                    if (j.iv) cipherParams.iv = CryptoJS.enc.Hex.parse(j.iv)
                    if (j.s) cipherParams.salt = CryptoJS.enc.Hex.parse(j.s)
                    return cipherParams;
                }
            }

            var script = get_between(c,"eval(function","{}))");
            script = "eval(function" + script + "{}))";
            var unpack = P_A_C_K_E_R.unpack(script);
            var data = unpack.split('var data=').pop().split(';')[0];
            eval("var data="+data+";");
            var dec = JSON.parse(CryptoJS.AES.decrypt(data, pass, {format: CryptoJSAesJson}).toString(CryptoJS.enc.Utf8));
            var res = P_A_C_K_E_R.unpack(dec);
            var vv = eval(res.split('player.setup({sources:').pop().split(',image:')[0]);
            var videos = [];
            for (var i = 0; i < vv.length; i++) {
                var item = {
                    url: vv[i]['file'],
                    name: vv[i]['label']
                };
                if(vv[i]['label'].includes("360"))videos.unshift(item);
                else videos.push(item);
            }
            // httpClient.postMessage(JSON.stringify({
            //             id: "CHECKSIZE",
            //             url: urlGFS + Base64.encode(JSON.stringify(videos)),
            //         }));
            var result = {
                videos: videos,
                subtitles: subtitles
            };
            setVideoInfo.postMessage(JSON.stringify(result));
            break;
        case "CHECKSIZE":
            var e = window.atob(content);
            var a = {
                videos: JSON.parse(e),
                subtitles: subtitles
            };
            setVideoInfo.postMessage(JSON.stringify(a));
            break;
        case "GET_VIDEO_INFO":
            var c = window.atob(content);
            console.dir(c);
            var ans = {};
            //
            // get value of which key is 'fmt_stream_map'.
            //
            var fmt_stream_map = getQueryVariable(c, 'fmt_stream_map');
            //
            // split my comma
            //
            var maps = fmt_stream_map.split("%2C");
            //
            // loop all links,
            //
            var purl = "";
            for (x in maps) {
                var res = decodeURIComponent(decodeURIComponent(maps[x])).split('|');
                // purl = res[1].replace(new RegExp("/\/[^\/]+\.google\.com/", 'g'),"//redirector.googlevideo.com/");
                // purl = res[1].replace(new RegExp("\.google\.com/", 'g'),".googlevideo.com/");

                purl = res[1];
                //.replace(/.c.docs.google.com/g,".googlevideo.com")
                //.replace(/app=explorer/g,"app=storage")
                //.replace(/key=ck2/g,"key=cms1")
                //.replace(/&cp=/g,"&api=")
                //.replace(/,cp&/g,',api&')
                //.replace(/,cp,/g,',api,')
                //.replace(/=cp,/g,'=api,')
                //.replace(/&sparams=/g,'&cms_redirect=yes&sparams=');

                switch (parseInt(res[0])) {
                    case 5:
                        quality = 'Low Quality, 240p, FLV, 400x240';
                        break;
                    case 17:
                        quality = 'Low Quality, 144p, 3GP, 0x0';
                        break;
                    case 18:
                        quality = 'Medium Quality, 360p, MP4, 480x360';
                        break;
                    case 22:
                        quality = 'High Quality, 720p, MP4, 1280x720';
                        break;
                    case 34:
                        quality = 'Medium Quality, 360p, FLV, 640x360';
                        break;
                    case 35:
                        quality = 'Standard Definition, 480p, FLV, 854x480';
                        break;
                    case 36:
                        quality = 'Low Quality, 240p, 3GP, 0x0';
                        break;
                    case 37:
                        quality = 'Full High Quality, 1080p, MP4, 1920x1080';
                        break;
                    case 38:
                        quality = 'Original Definition, MP4, 4096x3072';
                        break;
                    case 43:
                        quality = 'Medium Quality, 360p, WebM, 640x360';
                        break;
                    case 44:
                        quality = 'Standard Definition, 480p, WebM, 854x480';
                        break;
                    case 45:
                        quality = 'High Quality, 720p, WebM, 1280x720';
                        break;
                    case 46:
                        quality = 'Full High Quality, 1080p, WebM, 1280x720';
                        break;
                    case 82:
                        quality = 'Medium Quality 3D, 360p, MP4, 640x360';
                        break;
                    case 84:
                        quality = 'High Quality 3D, 720p, MP4, 1280x720';
                        break;
                    case 102:
                        quality = 'Medium Quality 3D, 360p, WebM, 640x360';
                        break;
                    case 104:
                        quality =  'High Quality 3D, 720p, WebM, 1280x720';
                        break;
                    default:
                        quality =  'transcoded (unknown) quality';
                        break;
                }
                ans[quality] = purl;
            }
            console.log(ans);

            url  = ans[Object.keys(ans)[0]];
            console.log(url);
            break;
    }

    return 1;
}
// getUrl.postMessage("");
function main() {
    getUrl.postMessage("");

    return 1;
}
function onHtmlResponse(url, content) {
    // testreturn();
    switch (url){
        case dlurl:
            var c = window.atob(content);
            var el = document.createElement('html');
            el.innerHTML = c;
            var x = el.getElementsByClassName('jfk-button-action');
            if(x.length>0){
                var confirm = x[0].getAttribute("href");
                if(confirm.substring(0,1)=="/"){
                    confirm = "https://drive.google.com"+confirm;
                }
                var key = get_between(confirm,"confirm=","&id");
                var id = confirm.split("id=").pop();
                var header = {

                };

                console.log(header);
                console.log(confirm);
                httpClient.postMessage(JSON.stringify({
                    id:"dlconfirm",
                    url: confirm,
                    header:header
                }));
            }
            break;
    }

    return 1;
}

function getQueryVariable(query, variable) {
    var vars = query.split('&');
    for (var i = 0; i < vars.length; i++) {
        var pair = vars[i].split('=');
        if (decodeURIComponent(pair[0]) == variable) {
            //return decodeURIComponent(pair[1]);
            return pair[1];
        }
    }
    console.log('Query variable %s not found', variable);
    return "";
}
var url1;
var url2;
var delayCloudFlare = 0;
var urlMain = "";
var urlIframe = "",
    subtitles=[],
    urlGFS = "http://robo.inflixer.com/v5/server/getfilesize.php?value=",
    videos=[],
    dlurl='';

main();
