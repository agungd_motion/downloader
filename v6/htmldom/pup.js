const puppeteer = require('puppeteer');
var myArgs = process.argv.slice(2);

(async () => {
    const browser = await puppeteer.launch();
    const page = await browser.newPage();
    var url = myArgs[0];
// url = "https://google.com";
try{
    await page.goto(url);
    await page.screenshot({path: 'example.png'});
    // const innerText = await page.evaluate(() => document.querySelector('body').);
    innerText = await page.$eval('video', video => video.getAttribute('src'));
    // innerText = await page.$eval('video', video => video.outerHTML);

    //const name = await page.content();
    console.log(innerText);
}catch(e){}
    await browser.close();
})();
