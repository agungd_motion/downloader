var P_A_C_K_E_R = {
    detect: function(a) {
        return P_A_C_K_E_R.get_chunks(a).length > 0
    },
    get_chunks: function(a) {
        var b = a.match(/eval\(\(?function\(.*?(,0,\{\}\)\)|split\('\|'\)\)\))($|\n)/g);
        return b || []
    },
    unpack: function(b) {
        for (var f, d = P_A_C_K_E_R.get_chunks(b), a = 0; a < d.length; a++) {
            f = d[a].replace(/\n$/, ""), b = b.split(f).join(P_A_C_K_E_R.unpack_chunk(f))
        }
        return b
    },
    unpack_chunk: function(e) {
        var c = "",
            n = eval;
        if (P_A_C_K_E_R.detect(e)) {
            try {
                eval = function(e) {
                    return c += e
                }, n(e), "string" == typeof c && c && (e = c)
            } catch (e) {}
        }
        return eval = n, e
    },
    run_tests: function(f) {
        var h = f || new SanityTest,
            g = "eval(function(p,a,c,k,e,r){e=String;if(!''.replace(/^/,String)){while(c--)r[c]=k[c]||c;k=[function(e){return r[e]}];e=function(){return'\\\\w+'};c=1};while(c--)if(k[c])p=p.replace(new RegExp('\\\\b'+e(c)+'\\\\b','g'),k[c]);return p}('0 2=1',3,3,'var||a'.split('|'),0,{}))",
            b = "eval(function(p,a,c,k,e,r){e=String;if(!''.replace(/^/,String)){while(c--)r[c]=k[c]||c;k=[function(e){return r[e]}];e=function(){return'\\\\w+'};c=1};while(c--)if(k[c])p=p.replace(new RegExp('\\\\b'+e(c)+'\\\\b','g'),k[c]);return p}('0 2=1',3,3,'foo||b'.split('|'),0,{}))",
            d = "eval(function(p,a,c,k,e,r){BORKBORK;if(!''.replace(/^/,String)){while(c--)r[c]=k[c]||c;k=[function(e){return r[e]}];e=function(){return'\\\\w+'};c=1};while(c--)if(k[c])p=p.replace(new RegExp('\\\\b'+e(c)+'\\\\b','g'),k[c]);return p}('0 2=1',3,3,'var||a'.split('|'),0,{}))";
        h.test_function(P_A_C_K_E_R.detect, "P_A_C_K_E_R.detect"), h.expect("", !1), h.expect("var a = b", !1), h.test_function(P_A_C_K_E_R.unpack, "P_A_C_K_E_R.unpack"), h.expect(d, d), h.expect(g, "var a=1"), h.expect(b, "foo b=1"), h.expect("eval(function(p,a,c,k,e,r){e=String;if(!''.replace(/^/,String)){while(c--)r[c]=k[c]||c;k=[function(e){return r[e]}];e=function(){return'\\\\w+'};c=1};while(c--)if(k[c])p=p.replace(new RegExp('\\\\b'+e(c)+'\\\\b','g'),k[c]);return p}('0 2=1{}))',3,3,'var||a'.split('|'),0,{}))", "var a=1{}))");
        var a = "\nfiller\n";
        return h.expect(a + g + "\n" + d + a + b + a, a + "var a=1\n" + d + a + "foo b=1" + a), h
    }
};

function get_between(c, b, a) {
    return c.substring(c.lastIndexOf(b) + b.length, c.lastIndexOf(a))
}

function onGetUrlResponse(a) {
    var c = a.split("?sub=");
    a = c[0];
    if (c.length > 1) {
        if (c[1] != "") {
            subtitles.push({
                url: c[1],
                name: "id"
            })
        }
    }
    var d = "FB21_MAIN";
    var b = a.trim();
    urlMain = b;
    urlReferrer = a;
    httpClient.postMessage(JSON.stringify({
        url: b,
        id: d
    }));
    return 1
}

function onHttpClientResponse(id, content) {
    switch (id) {
        case "FB21_MAIN":
            var decode = window.atob(content),
                found = 0,
                el = document.createElement("html");
            var clean = decode.replace(/\s+/g, "");
            el.innerHTML = decode;
            var x = el.getElementsByClassName("inner-wrap");
            if (x.length > 0) {}
            var labe = x[0].getElementsByClassName("server-item");
            for (var z = 0; z < labe.length; z++) {
                lab = labe[z].getElementsByClassName("server-title");
                var label = lab[0].innerText.trim();
                if (label.includes("Subtitle") != false) {
                    var urlnew = labe[z].getElementsByClassName("server-info");
                    var labeldl = urlnew[0].innerText;
                    if (labeldl.includes("Subtitle") != false) {
                        sub = urlnew[0].getElementsByTagName("a")[0].getAttribute("href");
                        var item = {
                            url: sub,
                            name: "default"
                        };
                        subtitles.push(item)
                    }
                }
            }
            //direct
            if (found == 0) {
                if (clean.indexOf("player.setup({sources:") >= 0) {
                    if (clean.indexOf("tracks:[") >= 0) {
                        var tvv = clean.split("player.setup({sources:").pop().split(",tracks:")[0];
                        sub = clean.split("tracks:").pop().split(",captions:")[0];
                        if (sub != "") {
                            var ss = eval(sub);
                            if (typeof ss === "object") {
                                for (var i = 0; i < ss.length; i++) {
                                    var item = {
                                        url: ss[i]["file"],
                                        name: ss[i]["label"]
                                    };
                                    subtitles.push(item)
                                }
                            }
                        }
                    } else {
                        var tvv = clean.split("player.setup({sources:").pop().split(",captions:")[0]
                    }
                    vv = eval(tvv);
                    if (typeof vv === "object") {
                        var videos = [];
                        for (var i = 0; i < vv.length; i++) {
                            var item = {
                                url: vv[i]["file"],
                                name: vv[i]["label"]
                            };
                            if (vv[i]["label"].includes("360")) {
                                videos.unshift(item)
                            } else {
                                videos.push(item)
                            }
                        }
                        if (videos.length > 0) {
                            found = 1;
                            var result = {
                                videos: videos,
                                subtitles: subtitles
                            };
                            setVideoInfo.postMessage(JSON.stringify(result));
                            return 0
                        }
                    }
                }
            }
            if (found == 0) {
                for (var z = 0; z < labe.length; z++) {
                    lab = labe[z].getElementsByClassName("server-title");
                    var label = lab[0].innerText;
                    if (label.includes("Alternatif") != false) {
                        var urlnew = labe[z].getElementsByClassName("server-info");
                        var labeldl = urlnew[0].innerText;
                        if(labeldl.includes("FEMBED")!=false){
                            for (var i = 0; i < urlnew. length; i++) {
                                var url1 = urlnew[i].getElementsByClassName('btn-sm');
                                if (urlnew != undefined) {
                                    found = 1
                                    url = url1[1].getAttribute('href');
                                    getFembed(url);

                                }
                            }
                            break;
                        }
                        // else if (labeldl.includes("GOUNLIMITED") != false) {
                        //     for (var i = 0; i < urlnew.length; i++) {
                        //         var url1 = urlnew[i].getElementsByClassName("btn-sm");
                        //         if (urlnew != undefined) {
                        //             found = 1;
                        //             id = "FB21_GU";
                        //             url = url1[1].getAttribute("href");
                        //             var newUrl1 = "https://api.inflixer.com/v4/go?api_key=2y10ZeA82P0VHPkwoRsv8soQeuBhjeJVHT2LjbymQqCFty8rJqFsz2y&m=g&url=";
                        //             var json = JSON.stringify({
                        //                 id: id,
                        //                 url: url
                        //             });
                        //             httpClient.postMessage(json)
                        //         }
                        //     }
                        // }
                    }
                }
            }
            if (found == 0) {
                url = decode.split('var str = "<iframe src="').pop().split('" frameborder')[0];
                if (url.indexOf("//gdriveplayer") >= 0) {
                    if (!url.includes("http")) {
                        url = "http:" + url
                    }
                    found = 1;
                    id = "FB21_IFRAME";
                    var json = JSON.stringify({
                        id: id,
                        url: url
                    });
                    httpClient.postMessage(json)
                }
            }

            if (found == 0) {
                if (x.length > 0) {
                    x = x[0].getElementsByTagName("h3");
                    if (x.length > 0) {
                        for (var w = 0; w < x.length; w++) {
                            var y = x[w].getElementsByClassName("btn-primary");
                            if (y.length > 0) {
                                urlTwo = y[0].getAttribute("href");
                                if (urlTwo.indexOf("cogihot") >= 0) {
                                    found = 1;
                                    urlTwo = encodeURI(urlTwo);
                                    id = "FB21_DL_PAGE";
                                    var json = JSON.stringify({
                                        id: id,
                                        url: urlTwo
                                    });
                                    httpClient.postMessage(json)
                                }
                            }
                        }
                    }
                }
            }

            break;
        case "FB21_GU":
            var decode = window.atob(content);
            var idoc;
            var videos = [];
            var el = document.createElement("html");
            el.innerHTML = decode;
            var newx = el.getElementsByTagName("script");
            if (newx.length > 0) {
                var label;
                for (var i = 0; i < newx.length; i++) {
                    var newurl1 = newx[i].innerText;
                    if (newurl1.includes("gounlimited")) {
                        newurl = newurl1
                    }
                }
                var unpack = P_A_C_K_E_R.unpack(newurl);
                var clean = unpack.replace(/\\s+/g, "");
                var vv = get_between(clean, 'sources:["', '"]');
                var item = {
                    url: vv,
                    name: "FB21"
                };
                videos.push(item);
                var result = {
                    videos: videos,
                    subtitles: []
                };
                setVideoInfo.postMessage(JSON.stringify(result))
            }
            break;
        case "FB21_DL_PAGE":
            var decode = window.atob(content);
            var el = document.createElement("html");
            el.innerHTML = decode;
            var videos = [];
            var newx1 = el.getElementsByTagName("iframe");
            if (newx1.length > 0) {
                url = newx1[0].getAttribute("src");
                if (!url.includes("http")) {
                    url = "http:" + url
                }
                url = url.replace("download.php", "embed2.php");
                id = "FB21_IFRAME";
                var json = JSON.stringify({
                    id: "FB21_IFRAME",
                    url: url
                })
            }
            httpClient.postMessage(json);
            break;
        case "FB21_IFRAME":
            var c = window.atob(content);
            // var pass = "alsfheafsjklNIWORNiolNIOWNKLNXakjsfwnBdwjbwfkjbJjkopfjweopjASoiwnrflakefneiofrt";
            // var CryptoJSAesJson = {
            //     stringify: function(cipherParams) {
            //         var j = {
            //             ct: cipherParams.ciphertext.toString(CryptoJS.enc.Base64)
            //         };
            //         if (cipherParams.iv) {
            //             j.iv = cipherParams.iv.toString()
            //         }
            //         if (cipherParams.salt) {
            //             j.s = cipherParams.salt.toString()
            //         }
            //         return JSON.stringify(j)
            //     },
            //     parse: function(jsonStr) {
            //         var j = JSON.parse(jsonStr);
            //         var cipherParams = CryptoJS.lib.CipherParams.create({
            //             ciphertext: CryptoJS.enc.Base64.parse(j.ct)
            //         });
            //         if (j.iv) {
            //             cipherParams.iv = CryptoJS.enc.Hex.parse(j.iv)
            //         }
            //         if (j.s) {
            //             cipherParams.salt = CryptoJS.enc.Hex.parse(j.s)
            //         }
            //         return cipherParams
            //     }
            // };
            // var script = get_between(c, "eval(function", "{}))");
            // script = "eval(function" + script + "{}))";
            // var unpack = P_A_C_K_E_R.unpack(script);
            // var data = unpack.split("var data=").pop().split(";")[0];
            // eval("var data=" + data + ";");
            // var dec = JSON.parse(CryptoJS.AES.decrypt(data, pass, {
            //     format: CryptoJSAesJson
            // }).toString(CryptoJS.enc.Utf8));
            // var res = P_A_C_K_E_R.unpack(dec);
            // var vv = eval(res.split("player.setup({sources:").pop().split(",image:")[0]);
            // var videos = [];
            // for (var i = 0; i < vv.length; i++) {
            //     var item = {
            //         url: vv[i]["file"],
            //         name: vv[i]["label"]
            //     };
            //     if (vv[i]["label"].includes("360")) {
            //         videos.unshift(item)
            //     } else {
            //         videos.push(item)
            //     }
            // }
            // var result = {
            //     videos: videos,
            //     subtitles: subtitles
            // };
            // setVideoInfo.postMessage(JSON.stringify(result));
            var vv = c.split("player.setup({").pop().split("image:")[0];
            var vv = vv.split("[").pop().split("]")[0];
            vv = '['+vv+']';
            vv = JSON.parse(vv);
            var videos = [];
            for (var i = 0; i < vv.length; i++) {
                var vurl = vv[i]["file"];
                if(!vurl.includes("http"))
                    vurl = 'http:'+vurl;
                var item = {
                    url: vurl,
                    name: vv[i]["label"]
                };
                if (vv[i]["label"].includes("360")) {
                    videos.unshift(item)
                } else {
                    videos.push(item)
                }
            }
            var result = {
                videos: videos,
                subtitles: subtitles
            };
            setVideoInfo.postMessage(JSON.stringify(result));
            break
        case "FEMBED":
            decode = window.atob(content);
            var j = JSON.parse(decode);
            var vv = j.data;
            var videos = [];
            for (var i = 0; i < vv.length; i++) {
                var item = {
                    url: vv[i]['file'],
                    name: vv[i]['label']
                };
                videos.push(item);
            }
            if(videos.length==0 && videos2.length>0){
                videos = videos2;
            }
            var result = {
                videos: videos,
                subtitles: subtitles
            };
            setVideoInfo.postMessage(JSON.stringify(result));
            break;
    }
    return 1
}
function getFembed(url) {
    var t = url.split("/"),
        r = t[t.length - 1];
    newUrl = "https://feurl.com/api/source/" + r.trim();
    var n = JSON.stringify({
        id: "FEMBED",
        url: newUrl,
        method: "POST"
    });
    httpClient.postMessage(n)
}
var delayCloudFlare = 0,
    urlMain = "",
    urlIframe = "",
    urlOne = "",
    urlTwo = "",
    decode = "",
    urlReferrer = "",
    urlLoader = "",
    sub = "",
    subtitles = [];

function main() {
    getUrl.postMessage("");
    return 1
}
main();