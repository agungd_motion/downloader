function get_between(str, first, last) {
    return str.substring(str.lastIndexOf(first) + first.length, str.lastIndexOf(last));
}
function onGetUrlResponse(url) {
    var purl = url.split("?sub=");
    url = purl[0];
    if (purl.length > 1) {
        if(purl[1]!=''){
            subtitles.push({
                url: purl[1],
                name: "id"
            })
        }
    }
    if(url.indexOf("_dev.")>=0){
        url = url.replace("_dev","");
    }
    var rpath = url.split("/");
    baseurl=rpath[0]+"//"+rpath[2];
    var newUrl = url;
    urlMain = newUrl;
    urlReferrer = url;
    if(url.indexOf("embed.dramacool.video")>=0){
        httpClient.postMessage(JSON.stringify({
            id: "ED_MAIN",
            url:newUrl,
        }));
    }

    return 1;
}
function onHttpClientResponse(id, content) {
    switch(id) {
        case "ED_MAIN":
            decode = window.atob(content);
            var clean = decode.replace(/\s+/g, "");

            var vv = get_between(clean,'varcountcheck=0;playerInstance.setup({sources:',',});playerInstance.on');
            var vv2 = get_between(clean,'playerInstance.setup({sources:',',});');
            vv = eval(vv);
            vv2 = eval(vv2);
            var videos = [];
            if(typeof vv == "object"){
                for (var i = 0; i < vv.length; i++) {
                    var item = {
                        url: vv[i]['file'],
                        name: vv[i]['label']
                    };
                    if(vv[i]['label'].includes("360"))videos.unshift(item);
                    else videos.push(item);
                }
            }

            if(typeof vv2 == "object"){
                for (var i = 0; i < vv2.length; i++) {
                    var item = {
                        url: vv2[i]['file'],
                        name: vv2[i]['label']
                    };
                    videos.push(item);
                }
            }

            if(videos.length>0){
                var result = {
                    videos: videos,
                    subtitles: subtitles
                }
                setVideoInfo.postMessage(JSON.stringify(result));
            }
            
            break;
        case "SK_IFRAME":
            decode = window.atob(content);
            var clean = decode.replace(/\s+/g, "");
            var vv = get_between(clean,'playerInstance.setup({sources:',',tracks:');
            var ss = get_between(clean,',tracks:',',skin:');
            vv = eval(vv);
            ss = eval(ss);

            var videos = [];
            for (var i = 0; i < vv.length; i++) {
                var item = {
                    url: vv[i]['file'],
                    name: vv[i]['label']
                };
                if(vv[i]['label'].includes("360"))videos.unshift(item);
                else videos.push(item);
            }

            for (var i = 0; i < ss.length; i++) {
                var surl = ss[i]['file'];
                if(surl.indexOf("http")<0)surl = baseurl + surl;
                var item = {
                    url: surl,
                    name: ss[i]['label']
                };
                subtitles.push(item);
            }

            var result = {
                videos: videos,
                subtitles: subtitles
            }
            setVideoInfo.postMessage(JSON.stringify(result));
            break;
        case "FEMBED":
            decode = Base64.decode(content);
            var j = JSON.parse(decode);
            var vv = j.data;
            var videos = [];
            for (var i = 0; i < vv.length; i++) {
                var item = {
                    url: vv[i]['file'],
                    name: vv[i]['label']
                };
                videos.push(item);
            }
            var result = {
                videos: videos,
                subtitles: subtitles
            };
            setVideoInfo.postMessage(JSON.stringify(result));
            break;
    }
    return 1;
}
function getFembed(url) {
    var t = url.split("/"),
        r = t[t.length - 1];
    newUrl = "https://feurl.com/api/source/" + r;
    var n = JSON.stringify({
        id: "FEMBED",
        url: newUrl,
        method: "POST"
    });
    httpClient.postMessage(n)
}

var delayCloudFlare = 0,
    urlIframe = "",
    baseurl = "",
    urlOne = "",
    urlTwo = "",
    decode = "",
    urlReferrer = "",
    urlLoader = "",
    sub = "",
    log = {},
    subtitles = [],
    iframeurl = [];

function main(){
    getUrl.postMessage("");
    return 1;
}
main();