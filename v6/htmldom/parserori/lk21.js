var P_A_C_K_E_R={detect:function(a){return P_A_C_K_E_R.get_chunks(a).length>0},get_chunks:function(a){var b=a.match(/eval\(\(?function\(.*?(,0,\{\}\)\)|split\('\|'\)\)\))($|\n)/g);return b||[]},unpack:function(b){for(var f,d=P_A_C_K_E_R.get_chunks(b),a=0;a<d.length;a++){f=d[a].replace(/\n$/,""),b=b.split(f).join(P_A_C_K_E_R.unpack_chunk(f))}return b},unpack_chunk:function(e){var c="",n=eval;if(P_A_C_K_E_R.detect(e)){try{eval=function(e){return c+=e},n(e),"string"==typeof c&&c&&(e=c)}catch(e){}}return eval=n,e},run_tests:function(f){var h=f||new SanityTest,g="eval(function(p,a,c,k,e,r){e=String;if(!''.replace(/^/,String)){while(c--)r[c]=k[c]||c;k=[function(e){return r[e]}];e=function(){return'\\\\w+'};c=1};while(c--)if(k[c])p=p.replace(new RegExp('\\\\b'+e(c)+'\\\\b','g'),k[c]);return p}('0 2=1',3,3,'var||a'.split('|'),0,{}))",b="eval(function(p,a,c,k,e,r){e=String;if(!''.replace(/^/,String)){while(c--)r[c]=k[c]||c;k=[function(e){return r[e]}];e=function(){return'\\\\w+'};c=1};while(c--)if(k[c])p=p.replace(new RegExp('\\\\b'+e(c)+'\\\\b','g'),k[c]);return p}('0 2=1',3,3,'foo||b'.split('|'),0,{}))",d="eval(function(p,a,c,k,e,r){BORKBORK;if(!''.replace(/^/,String)){while(c--)r[c]=k[c]||c;k=[function(e){return r[e]}];e=function(){return'\\\\w+'};c=1};while(c--)if(k[c])p=p.replace(new RegExp('\\\\b'+e(c)+'\\\\b','g'),k[c]);return p}('0 2=1',3,3,'var||a'.split('|'),0,{}))";h.test_function(P_A_C_K_E_R.detect,"P_A_C_K_E_R.detect"),h.expect("",!1),h.expect("var a = b",!1),h.test_function(P_A_C_K_E_R.unpack,"P_A_C_K_E_R.unpack"),h.expect(d,d),h.expect(g,"var a=1"),h.expect(b,"foo b=1"),h.expect("eval(function(p,a,c,k,e,r){e=String;if(!''.replace(/^/,String)){while(c--)r[c]=k[c]||c;k=[function(e){return r[e]}];e=function(){return'\\\\w+'};c=1};while(c--)if(k[c])p=p.replace(new RegExp('\\\\b'+e(c)+'\\\\b','g'),k[c]);return p}('0 2=1{}))',3,3,'var||a'.split('|'),0,{}))","var a=1{}))");var a="\nfiller\n";return h.expect(a+g+"\n"+d+a+b+a,a+"var a=1\n"+d+a+"foo b=1"+a),h}};var CryptoJS=CryptoJS||function(b,t){var q={},r=q.lib={},v=function(){},w=r.Base={extend:function(a){v.prototype=this;var c=new v;a&&c.mixIn(a);c.hasOwnProperty("init")||(c.init=function(){c.$super.init.apply(this,arguments)});c.init.prototype=c;c.$super=this;return c},create:function(){var a=this.extend();a.init.apply(a,arguments);return a},init:function(){},mixIn:function(a){for(var c in a){a.hasOwnProperty(c)&&(this[c]=a[c])}a.hasOwnProperty("toString")&&(this.toString=a.toString)},clone:function(){return this.init.prototype.extend(this)}},x=r.WordArray=w.extend({init:function(a,c){a=this.words=a||[];this.sigBytes=c!=t?c:4*a.length},toString:function(a){return(a||d).stringify(this)},concat:function(a){var e=this.words,g=a.words,c=this.sigBytes;a=a.sigBytes;this.clamp();if(c%4){for(var f=0;f<a;f++){e[c+f>>>2]|=(g[f>>>2]>>>24-8*(f%4)&255)<<24-8*((c+f)%4)}}else{if(65535<g.length){for(f=0;f<a;f+=4){e[c+f>>>2]=g[f>>>2]}}else{e.push.apply(e,g)}}this.sigBytes+=a;return this},clamp:function(){var a=this.words,c=this.sigBytes;a[c>>>2]&=4294967295<<32-8*(c%4);a.length=b.ceil(c/4)},clone:function(){var a=w.clone.call(this);a.words=this.words.slice(0);return a},random:function(a){for(var c=[],e=0;e<a;e+=4){c.push(4294967296*b.random()|0)}return new x.init(c,a)}}),l=q.enc={},d=l.Hex={stringify:function(a){var e=a.words;a=a.sigBytes;for(var g=[],c=0;c<a;c++){var f=e[c>>>2]>>>24-8*(c%4)&255;g.push((f>>>4).toString(16));g.push((f&15).toString(16))}return g.join("")},parse:function(a){for(var f=a.length,c=[],e=0;e<f;e+=2){c[e>>>3]|=parseInt(a.substr(e,2),16)<<24-4*(e%8)}return new x.init(c,f/2)}},p=l.Latin1={stringify:function(a){var f=a.words;a=a.sigBytes;for(var c=[],e=0;e<a;e++){c.push(String.fromCharCode(f[e>>>2]>>>24-8*(e%4)&255))}return c.join("")},parse:function(a){for(var f=a.length,c=[],e=0;e<f;e++){c[e>>>2]|=(a.charCodeAt(e)&255)<<24-8*(e%4)}return new x.init(c,f)}},n=l.Utf8={stringify:function(a){try{return decodeURIComponent(escape(p.stringify(a)))}catch(c){throw Error("Malformed UTF-8 data")}},parse:function(a){return p.parse(unescape(encodeURIComponent(a)))}},u=r.BufferedBlockAlgorithm=w.extend({reset:function(){this._data=new x.init;this._nDataBytes=0},_append:function(a){"string"==typeof a&&(a=n.parse(a));this._data.concat(a);this._nDataBytes+=a.sigBytes},_process:function(a){var c=this._data,e=c.words,g=c.sigBytes,i=this.blockSize,h=g/(4*i),h=a?b.ceil(h):b.max((h|0)-this._minBufferSize,0);a=h*i;g=b.min(4*a,g);if(a){for(var f=0;f<a;f+=i){this._doProcessBlock(e,f)}f=e.splice(0,a);c.sigBytes-=g}return new x.init(f,g)},clone:function(){var a=w.clone.call(this);a._data=this._data.clone();return a},_minBufferSize:0});r.Hasher=u.extend({cfg:w.extend(),init:function(a){this.cfg=this.cfg.extend(a);this.reset()},reset:function(){u.reset.call(this);this._doReset()},update:function(a){this._append(a);this._process();return this},finalize:function(a){a&&this._append(a);return this._doFinalize()},blockSize:16,_createHelper:function(a){return function(e,c){return(new a.init(c)).finalize(e)}},_createHmacHelper:function(a){return function(e,c){return(new s.HMAC.init(a,c)).finalize(e)}}});var s=q.algo={};return q}(Math);(function(){var d=CryptoJS,c=d.lib.WordArray;d.enc.Base64={stringify:function(a){var n=a.words,b=a.sigBytes,l=this._map;a.clamp();a=[];for(var k=0;k<b;k+=3){for(var o=(n[k>>>2]>>>24-8*(k%4)&255)<<16|(n[k+1>>>2]>>>24-8*((k+1)%4)&255)<<8|n[k+2>>>2]>>>24-8*((k+2)%4)&255,m=0;4>m&&k+0.75*m<b;m++){a.push(l.charAt(o>>>6*(3-m)&63))}}if(n=l.charAt(64)){for(;a.length%4;){a.push(n)}}return a.join("")},parse:function(a){var o=a.length,l=this._map,m=l.charAt(64);m&&(m=a.indexOf(m),-1!=m&&(o=m));for(var m=[],b=0,p=0;p<o;p++){if(p%4){var n=l.indexOf(a.charAt(p-1))<<2*(p%4),q=l.indexOf(a.charAt(p))>>>6-2*(p%4);m[b>>>2]|=(n|q)<<24-8*(b%4);b++}}return c.create(m,b)},_map:"ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/="}})();(function(l){function u(i,c,a,e,f,g,h){i=i+(c&a|~c&e)+f+h;return(i<<g|i>>>32-g)+c}function s(i,c,a,e,f,g,h){i=i+(c&e|a&~e)+f+h;return(i<<g|i>>>32-g)+c}function t(i,c,a,e,f,g,h){i=i+(c^a^e)+f+h;return(i<<g|i>>>32-g)+c}function b(i,c,a,e,f,g,h){i=i+(a^(c|~e))+f+h;return(i<<g|i>>>32-g)+c}for(var d=CryptoJS,v=d.lib,p=v.WordArray,o=v.Hasher,v=d.algo,r=[],q=0;64>q;q++){r[q]=4294967296*l.abs(l.sin(q+1))|0}v=v.MD5=o.extend({_doReset:function(){this._hash=new p.init([1732584193,4023233417,2562383102,271733878])},_doProcessBlock:function(w,m){for(var a=0;16>a;a++){var c=m+a,e=w[c];w[c]=(e<<8|e>>>24)&16711935|(e<<24|e>>>8)&4278255360}var a=this._hash.words,c=w[m+0],e=w[m+1],i=w[m+2],j=w[m+3],Y=w[m+4],A=w[m+5],x=w[m+6],W=w[m+7],V=w[m+8],n=w[m+9],z=w[m+10],B=w[m+11],E=w[m+12],C=w[m+13],D=w[m+14],X=w[m+15],f=a[0],k=a[1],g=a[2],h=a[3],f=u(f,k,g,h,c,7,r[0]),h=u(h,f,k,g,e,12,r[1]),g=u(g,h,f,k,i,17,r[2]),k=u(k,g,h,f,j,22,r[3]),f=u(f,k,g,h,Y,7,r[4]),h=u(h,f,k,g,A,12,r[5]),g=u(g,h,f,k,x,17,r[6]),k=u(k,g,h,f,W,22,r[7]),f=u(f,k,g,h,V,7,r[8]),h=u(h,f,k,g,n,12,r[9]),g=u(g,h,f,k,z,17,r[10]),k=u(k,g,h,f,B,22,r[11]),f=u(f,k,g,h,E,7,r[12]),h=u(h,f,k,g,C,12,r[13]),g=u(g,h,f,k,D,17,r[14]),k=u(k,g,h,f,X,22,r[15]),f=s(f,k,g,h,e,5,r[16]),h=s(h,f,k,g,x,9,r[17]),g=s(g,h,f,k,B,14,r[18]),k=s(k,g,h,f,c,20,r[19]),f=s(f,k,g,h,A,5,r[20]),h=s(h,f,k,g,z,9,r[21]),g=s(g,h,f,k,X,14,r[22]),k=s(k,g,h,f,Y,20,r[23]),f=s(f,k,g,h,n,5,r[24]),h=s(h,f,k,g,D,9,r[25]),g=s(g,h,f,k,j,14,r[26]),k=s(k,g,h,f,V,20,r[27]),f=s(f,k,g,h,C,5,r[28]),h=s(h,f,k,g,i,9,r[29]),g=s(g,h,f,k,W,14,r[30]),k=s(k,g,h,f,E,20,r[31]),f=t(f,k,g,h,A,4,r[32]),h=t(h,f,k,g,V,11,r[33]),g=t(g,h,f,k,B,16,r[34]),k=t(k,g,h,f,D,23,r[35]),f=t(f,k,g,h,e,4,r[36]),h=t(h,f,k,g,Y,11,r[37]),g=t(g,h,f,k,W,16,r[38]),k=t(k,g,h,f,z,23,r[39]),f=t(f,k,g,h,C,4,r[40]),h=t(h,f,k,g,c,11,r[41]),g=t(g,h,f,k,j,16,r[42]),k=t(k,g,h,f,x,23,r[43]),f=t(f,k,g,h,n,4,r[44]),h=t(h,f,k,g,E,11,r[45]),g=t(g,h,f,k,X,16,r[46]),k=t(k,g,h,f,i,23,r[47]),f=b(f,k,g,h,c,6,r[48]),h=b(h,f,k,g,W,10,r[49]),g=b(g,h,f,k,D,15,r[50]),k=b(k,g,h,f,A,21,r[51]),f=b(f,k,g,h,E,6,r[52]),h=b(h,f,k,g,j,10,r[53]),g=b(g,h,f,k,z,15,r[54]),k=b(k,g,h,f,e,21,r[55]),f=b(f,k,g,h,V,6,r[56]),h=b(h,f,k,g,X,10,r[57]),g=b(g,h,f,k,x,15,r[58]),k=b(k,g,h,f,C,21,r[59]),f=b(f,k,g,h,Y,6,r[60]),h=b(h,f,k,g,B,10,r[61]),g=b(g,h,f,k,i,15,r[62]),k=b(k,g,h,f,n,21,r[63]);a[0]=a[0]+f|0;a[1]=a[1]+k|0;a[2]=a[2]+g|0;a[3]=a[3]+h|0},_doFinalize:function(){var g=this._data,c=g.words,a=8*this._nDataBytes,e=8*g.sigBytes;c[e>>>5]|=128<<24-e%32;var f=l.floor(a/4294967296);c[(e+64>>>9<<4)+15]=(f<<8|f>>>24)&16711935|(f<<24|f>>>8)&4278255360;c[(e+64>>>9<<4)+14]=(a<<8|a>>>24)&16711935|(a<<24|a>>>8)&4278255360;g.sigBytes=4*(c.length+1);this._process();g=this._hash;c=g.words;for(a=0;4>a;a++){e=c[a],c[a]=(e<<8|e>>>24)&16711935|(e<<24|e>>>8)&4278255360}return g},clone:function(){var a=o.clone.call(this);a._hash=this._hash.clone();return a}});d.MD5=o._createHelper(v);d.HmacMD5=o._createHmacHelper(v)})(Math);(function(){var d=CryptoJS,i=d.lib,h=i.Base,g=i.WordArray,i=d.algo,j=i.EvpKDF=h.extend({cfg:h.extend({keySize:4,hasher:i.MD5,iterations:1}),init:function(a){this.cfg=this.cfg.extend(a)},compute:function(e,q){for(var n=this.cfg,r=n.hasher.create(),c=g.create(),a=c.words,p=n.keySize,n=n.iterations;a.length<p;){f&&r.update(f);var f=r.update(e).finalize(q);r.reset();for(var b=1;b<n;b++){f=r.finalize(f),r.reset()}c.concat(f)}c.sigBytes=4*p;return c}});d.EvpKDF=function(a,c,b){return j.create(b).compute(a,c)}})();CryptoJS.lib.Cipher||function(c){var t=CryptoJS,q=t.lib,r=q.Base,a=q.WordArray,b=q.BufferedBlockAlgorithm,v=t.enc.Base64,w=t.algo.EvpKDF,l=q.Cipher=b.extend({cfg:r.extend(),createEncryptor:function(f,e){return this.create(this._ENC_XFORM_MODE,f,e)},createDecryptor:function(f,e){return this.create(this._DEC_XFORM_MODE,f,e)},init:function(e,f,g){this.cfg=this.cfg.extend(g);this._xformMode=e;this._key=f;this.reset()},reset:function(){b.reset.call(this);this._doReset()},process:function(e){this._append(e);return this._process()},finalize:function(e){e&&this._append(e);return this._doFinalize()},keySize:4,ivSize:4,_ENC_XFORM_MODE:1,_DEC_XFORM_MODE:2,_createHelper:function(e){return{encrypt:function(h,f,g){return("string"==typeof f?p:d).encrypt(e,h,f,g)},decrypt:function(h,f,g){return("string"==typeof f?p:d).decrypt(e,h,f,g)}}}});q.StreamCipher=l.extend({_doFinalize:function(){return this._process(!0)},blockSize:1});var n=t.mode={},x=function(h,i,e){var f=this._iv;f?this._iv=c:f=this._prevBlock;for(var g=0;g<e;g++){h[i+g]^=f[g]}},u=(q.BlockCipherMode=r.extend({createEncryptor:function(f,e){return this.Encryptor.create(f,e)},createDecryptor:function(f,e){return this.Decryptor.create(f,e)},init:function(f,e){this._cipher=f;this._iv=e}})).extend();u.Encryptor=u.extend({processBlock:function(h,e){var g=this._cipher,f=g.blockSize;x.call(this,h,e,f);g.encryptBlock(h,e);this._prevBlock=h.slice(e,e+f)}});u.Decryptor=u.extend({processBlock:function(h,i){var e=this._cipher,f=e.blockSize,g=h.slice(i,i+f);e.decryptBlock(h,i);x.call(this,h,i,f);this._prevBlock=g}});n=n.CBC=u;u=(t.pad={}).Pkcs7={pad:function(h,j){for(var e=4*j,e=e-h.sigBytes%e,g=e<<24|e<<16|e<<8|e,i=[],f=0;f<e;f+=4){i.push(g)}e=a.create(i,e);h.concat(e)},unpad:function(e){e.sigBytes-=e.words[e.sigBytes-1>>>2]&255}};q.BlockCipher=l.extend({cfg:l.cfg.extend({mode:n,padding:u}),reset:function(){l.reset.call(this);var e=this.cfg,f=e.iv,e=e.mode;if(this._xformMode==this._ENC_XFORM_MODE){var g=e.createEncryptor}else{g=e.createDecryptor,this._minBufferSize=1}this._mode=g.call(e,this,f&&f.words)},_doProcessBlock:function(e,f){this._mode.processBlock(e,f)},_doFinalize:function(){var e=this.cfg.padding;if(this._xformMode==this._ENC_XFORM_MODE){e.pad(this._data,this.blockSize);var f=this._process(!0)}else{f=this._process(!0),e.unpad(f)}return f},blockSize:4});var s=q.CipherParams=r.extend({init:function(e){this.mixIn(e)},toString:function(e){return(e||this.formatter).stringify(this)}}),n=(t.format={}).OpenSSL={stringify:function(e){var f=e.ciphertext;e=e.salt;return(e?a.create([1398893684,1701076831]).concat(e).concat(f):f).toString(v)},parse:function(e){e=v.parse(e);var f=e.words;if(1398893684==f[0]&&1701076831==f[1]){var g=a.create(f.slice(2,4));f.splice(0,4);e.sigBytes-=16}return s.create({ciphertext:e,salt:g})}},d=q.SerializableCipher=r.extend({cfg:r.extend({format:n}),encrypt:function(g,i,e,f){f=this.cfg.extend(f);var h=g.createEncryptor(e,f);i=h.finalize(i);h=h.cfg;return s.create({ciphertext:i,key:e,iv:h.iv,algorithm:g,mode:h.mode,padding:h.padding,blockSize:g.blockSize,formatter:f.format})},decrypt:function(h,e,f,g){g=this.cfg.extend(g);e=this._parse(e,g.format);return h.createDecryptor(f,g).finalize(e.ciphertext)},_parse:function(e,f){return"string"==typeof e?f.parse(e,this):e}}),t=(t.kdf={}).OpenSSL={execute:function(h,e,f,g){g||(g=a.random(8));h=w.create({keySize:e+f}).compute(h,g);f=a.create(h.words.slice(e),4*f);h.sigBytes=4*e;return s.create({key:h,iv:f,salt:g})}},p=q.PasswordBasedCipher=d.extend({cfg:d.cfg.extend({kdf:t}),encrypt:function(e,f,g,h){h=this.cfg.extend(h);g=h.kdf.execute(g,e.keySize,e.ivSize);h.iv=g.iv;e=d.encrypt.call(this,e,f,g.key,h);e.mixIn(g);return e},decrypt:function(e,f,g,h){h=this.cfg.extend(h);f=this._parse(f,h.format);g=h.kdf.execute(g,e.keySize,e.ivSize,f.salt);h.iv=g.iv;return d.decrypt.call(this,e,f,g.key,h)}})}();(function(){for(var u=CryptoJS,p=u.lib.BlockCipher,d=u.algo,l=[],s=[],t=[],r=[],y=[],w=[],b=[],F=[],q=[],n=[],a=[],c=0;256>c;c++){a[c]=128>c?c<<1:c<<1^283}for(var e=0,j=0,c=0;256>c;c++){var k=j^j<<1^j<<2^j<<3^j<<4,k=k>>>8^k&255^99;l[e]=k;s[k]=e;var H=a[e],v=a[H],x=a[v],G=257*a[k]^16843008*k;t[e]=G<<24|G>>>8;r[e]=G<<16|G>>>16;y[e]=G<<8|G>>>24;w[e]=G;G=16843009*x^65537*v^257*H^16843008*e;b[k]=G<<24|G>>>8;F[k]=G<<16|G>>>16;q[k]=G<<8|G>>>24;n[k]=G;e?(e=H^a[a[a[x^H]]],j^=a[a[j]]):e=j=1}var z=[0,1,2,4,8,16,32,64,128,27,54],d=d.AES=p.extend({_doReset:function(){for(var f=this._key,h=f.words,i=f.sigBytes/4,f=4*((this._nRounds=i+6)+1),m=this._keySchedule=[],o=0;o<f;o++){if(o<i){m[o]=h[o]}else{var g=m[o-1];o%i?6<i&&4==o%i&&(g=l[g>>>24]<<24|l[g>>>16&255]<<16|l[g>>>8&255]<<8|l[g&255]):(g=g<<8|g>>>24,g=l[g>>>24]<<24|l[g>>>16&255]<<16|l[g>>>8&255]<<8|l[g&255],g^=z[o/i|0]<<24);m[o]=m[o-i]^g}}h=this._invKeySchedule=[];for(i=0;i<f;i++){o=f-i,g=i%4?m[o]:m[o-4],h[i]=4>i||4>=o?g:b[l[g>>>24]]^F[l[g>>>16&255]]^q[l[g>>>8&255]]^n[l[g&255]]}},encryptBlock:function(f,g){this._doCryptBlock(f,g,this._keySchedule,t,r,y,w,l)},decryptBlock:function(f,g){var h=f[g+1];f[g+1]=f[g+3];f[g+3]=h;this._doCryptBlock(f,g,this._invKeySchedule,b,F,q,n,s);h=f[g+1];f[g+1]=f[g+3];f[g+3]=h},_doCryptBlock:function(M,N,O,f,g,o,I,h){for(var L=this._nRounds,i=M[N]^O[0],m=M[N+1]^O[1],D=M[N+2]^O[2],A=M[N+3]^O[3],B=4,E=1;E<L;E++){var C=f[i>>>24]^g[m>>>16&255]^o[D>>>8&255]^I[A&255]^O[B++],J=f[m>>>24]^g[D>>>16&255]^o[A>>>8&255]^I[i&255]^O[B++],K=f[D>>>24]^g[A>>>16&255]^o[i>>>8&255]^I[m&255]^O[B++],A=f[A>>>24]^g[i>>>16&255]^o[m>>>8&255]^I[D&255]^O[B++],i=C,m=J,D=K}C=(h[i>>>24]<<24|h[m>>>16&255]<<16|h[D>>>8&255]<<8|h[A&255])^O[B++];J=(h[m>>>24]<<24|h[D>>>16&255]<<16|h[A>>>8&255]<<8|h[i&255])^O[B++];K=(h[D>>>24]<<24|h[A>>>16&255]<<16|h[i>>>8&255]<<8|h[m&255])^O[B++];A=(h[A>>>24]<<24|h[i>>>16&255]<<16|h[m>>>8&255]<<8|h[D&255])^O[B++];M[N]=C;M[N+1]=J;M[N+2]=K;M[N+3]=A},keySize:8});u.AES=p._createHelper(d)})();
function get_between(c, b, a) {
    return c.substring(c.lastIndexOf(b) + b.length, c.lastIndexOf(a))
}

function onGetUrlResponse(a) {
    var c = a.split("?sub=");
    a = c[0];
    if (c.length > 1) {
        if (c[1] != "") {
            subtitles.push({
                url: c[1],
                name: "id"
            })
        }
    }
    if (a.indexOf("_dev.") >= 0) {
        a = a.replace("_dev", "")
    }
    var d = a.split("/");
    baseurl = d[0] + "//" + d[2];
    var b = a;
    urlMain = b;
    urlReferrer = a;
    httpClient.postMessage(JSON.stringify({
        id: "LK21_MAIN",
        url: b
    }));
    return 1
}

function onHttpClientResponse(id, content) {
    switch (id) {
        case "LK21_MAIN":
            decode = window.atob(content);
            var el = document.createElement("html");
            el.innerHTML = decode;
            var mp = el.getElementsByClassName("gmr-embed-responsive");
            if (mp.length > 0) {
                var iframe = mp[0].getElementsByTagName("iframe");
                if (iframe.length > 0) {
                    var url = iframe[0].getAttribute("src");
                    httpClient.postMessage(JSON.stringify({
                        id: "LK21_IFRAME",
                        url: url
                    }))
                }
            }
            break;
        case "LK21_IFRAME":
            decode = window.atob(content);
            var el = document.createElement("html");
            el.innerHTML = decode;
            var iframe = el.getElementsByTagName("iframe");
            if (iframe.length > 0) {
                var url = iframe[0].getAttribute("src");
                if (url.indexOf("//gdriveplayer") >= 0) {
                    if (!url.includes("http")) {
                        url = "http:" + url
                    }
                    if (url.includes("gdriveplayer.us")) {
                        url = url.replace("gdriveplayer.us","gdriveplayer.me")
                    }
                    found = 1;
                    httpClient.postMessage(JSON.stringify({
                        id: "GDRIVEPLAYER",
                        url: url
                    }))
                }
            }
            break;
        case "GDRIVEPLAYER":
            var c = window.atob(content);
            var pass = "alsfheafsjklNIWORNiolNIOWNKLNXakjsfwnBdwjbwfkjbJjkopfjweopjASoiwnrflakefneiofrt";
            var CryptoJSAesJson = {
                stringify: function(cipherParams) {
                    var j = {
                        ct: cipherParams.ciphertext.toString(CryptoJS.enc.Base64)
                    };
                    if (cipherParams.iv) {
                        j.iv = cipherParams.iv.toString()
                    }
                    if (cipherParams.salt) {
                        j.s = cipherParams.salt.toString()
                    }
                    return JSON.stringify(j)
                },
                parse: function(jsonStr) {
                    var j = JSON.parse(jsonStr);
                    var cipherParams = CryptoJS.lib.CipherParams.create({
                        ciphertext: CryptoJS.enc.Base64.parse(j.ct)
                    });
                    if (j.iv) {
                        cipherParams.iv = CryptoJS.enc.Hex.parse(j.iv)
                    }
                    if (j.s) {
                        cipherParams.salt = CryptoJS.enc.Hex.parse(j.s)
                    }
                    return cipherParams
                }
            };
            var script = get_between(c, "eval(function", "{}))");
            script = "eval(function" + script + "{}))";
            var unpack = P_A_C_K_E_R.unpack(script);
            var data = unpack.split("var data=").pop().split(";")[0];
            eval("var data=" + data + ";");
            var dec = JSON.parse(CryptoJS.AES.decrypt(data, pass, {
                format: CryptoJSAesJson
            }).toString(CryptoJS.enc.Utf8));
            var res = P_A_C_K_E_R.unpack(dec);
            var vv = eval(res.split("player.setup({sources:").pop().split(",image:")[0]);
            var ss = eval(res.split("tracks:").pop().split(",captions:")[0]);
            var videos = [];
            for (var i = 0; i < vv.length; i++) {
                var vurl = vv[i]["file"];
                if(!vurl.includes("http"))
                    vurl = 'http:'+vurl;
                if(vurl.substr(0,2)=="//")
                    vurl = 'http:'+vurl;
                var item = {
                    url: vurl,
                    name: vv[i]["label"]
                };
                if (vv[i]["label"].includes("360")) {
                    videos.unshift(item)
                } else {
                    videos.push(item)
                }
            }
            for(var i=0;i<ss.length;i++){
                var surl=ss[i]["file"];
                if(surl.indexOf("?subtitle=")>=0){
                    surl=surl.split("=").pop()
                }
                var item={url:surl,name:ss[i]["label"]};
                subtitles.push(item)
            }
            var result = {
                videos: videos,
                subtitles: subtitles
            };
            setVideoInfo.postMessage(JSON.stringify(result));
            break
    }
    return 1
}
var delayCloudFlare = 0,
    urlIframe = "",
    baseurl = "",
    urlOne = "",
    urlTwo = "",
    decode = "",
    urlReferrer = "",
    urlLoader = "",
    sub = "",
    log = {},
    subtitles = [],
    iframeurl = [];

function main() {
    getUrl.postMessage("");
    return 1
}
main();