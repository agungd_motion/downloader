function get_between(str, first, last) {
    return str.substring(str.lastIndexOf(first) + first.length, str.lastIndexOf(last));
}
function onGetUrlResponse(url) {
    var purl = url.split("?sub=");
    url = purl[0];
    if (purl.length > 1) {
        if(purl[1]!=''){
            subtitles.push({
                url: purl[1],
                name: "id"
            })
        }
    }
    if(url.indexOf("_dev.")>=0){
        url = url.replace("_dev","");
    }
    var rpath = url.split("/");
    baseurl=rpath[0]+"//"+rpath[2];
    var newUrl = url;
    urlMain = newUrl;
    urlReferrer = url;

    httpClient.postMessage(JSON.stringify({
        id: "DC9_MAIN",
        url:newUrl,
    }));

    return 1;
}
function onHttpClientResponse(id, content) {
    switch(id) {
        case "DC9_MAIN":
            decode = window.atob(content);
            var fembed = ['feurl','fembed','gcloud.live','xstreamcdn'];
            var el = document.createElement('html');
            el.innerHTML = decode;
            var slist = el.getElementsByClassName("anime_muti_link");

            if(slist.length>0){
                var lis = slist[0].getElementsByTagName("li");
                if(lis.length>0){
                    for(var i=0;i<lis.length;i++){
                        var ds = lis[i].getAttribute("data-video");
                        var ifurl = ds.trim();
                        if(ifurl.substr(0,2)=="//"){
                            ifurl = "http:" + ifurl ;
                        }
                        if(fembed.some(function(x){return ifurl.indexOf(x) > -1})){
                            iframeurl.unshift(ifurl);
                        }else iframeurl.push(ifurl);
                    }
                }

            }
            if(iframeurl.length>0){
                if(iframeurl[0].indexOf("embed.watchasian")>0){
                    httpClient.postMessage(JSON.stringify({
                        id: "embed.watchasian",
                        url:iframeurl[0]
                    }));
                }else{

                    // var cek = fembed.some(x => iframeurl[0].indexOf(x) > -1);
                    if(fembed.some(function(x){return iframeurl[0].indexOf(x) > -1})){
                        getFembed(iframeurl[0]);
                    }
                }
            }
            break;
        case "embed.watchasian":
            decode = window.atob(content);
            var clean = decode.replace(/\s+/g, "");
            var vv = get_between(clean,'varcountcheck=0;playerInstance.setup({sources:',',});playerInstance.on');
            var vv2 = get_between(clean,'playerInstance.setup({sources:',',});');
            if(vv.indexOf(",tracks:")>=0){
                vv = vv.substr(0,vv.indexOf(",tracks:"));
            }
            if(vv2.indexOf(",tracks:")>=0){
                vv2 = vv2.substr(0,vv2.indexOf(",tracks:"));
            }
            vv = eval(vv);
            vv2 = eval(vv2);
            var videos = [];
            if(typeof vv == "object"){
                for (var i = 0; i < vv.length; i++) {
                    var item = {
                        url: vv[i]['file'],
                        name: vv[i]['label']
                    };
                    if(vv[i]['label'].includes("360"))videos.unshift(item);
                    else videos.push(item);
                }
            }

            if(typeof vv2 == "object"){
                for (var i = 0; i < vv2.length; i++) {
                    var item = {
                        url: vv2[i]['file'],
                        name: vv2[i]['label']
                    };
                    videos.push(item);
                }
            }

            if(videos.length>0){
                var result = {
                    videos: videos,
                    subtitles: subtitles
                }

                setVideoInfo.postMessage(JSON.stringify(result));
            }
            break;
        case "fembed":
            decode = window.atob(content);

            var j = JSON.parse(decode);
            var vv = j.data;
            var videos = [];
            for (var i = 0; i < vv.length; i++) {
                var item = {
                    url: vv[i]['file'],
                    name: vv[i]['label']
                };
                videos.push(item);
            }
            var result = {
                videos: videos,
                subtitles: subtitles
            };
            setVideoInfo.postMessage(JSON.stringify(result));
            break;
    }
    return 1;
}
function getFembed(url) {
    var t = url.split("/"),
        r = t[t.length - 1];
    var newUrl = "https://feurl.com/api/source/" + r;
    httpClient.postMessage(JSON.stringify({
        id: "fembed",
        url: newUrl,
        method: "POST"
    }));
}

var delayCloudFlare = 0,
    urlIframe = "",
    baseurl = "",
    urlOne = "",
    urlTwo = "",
    decode = "",
    urlReferrer = "",
    urlLoader = "",
    sub = "",
    log = {},
    subtitles = [],
    iframeurl = [];

function main(){
    getUrl.postMessage("");
    return 1;
}
main();