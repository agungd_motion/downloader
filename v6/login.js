/**
 * @name Google Social Login
 *
 * @desc Logs into Checkly using Google social Login. Provide your username and password as environment variables when running the script, i.e:
 * `GOOGLE_USER=myuser GOOGLE_PWD=mypassword node google_social.js`
 *
 */
const puppeteer = require('puppeteer');


(async () => {
    // set some options (set headless to false so we can see
    // this automated browsing experience)
    let launchOptions = {
        headless: false,
        // userDataDir: "./user_data"
    };
const browser = await puppeteer.launch(launchOptions)
const page = await browser.newPage()

await page.setViewport({ width: 1280, height: 800 })
await page.goto('https://www.metube.id/upload')

const navigationPromise = page.waitForNavigation()
console.log(page.url());
if(page.url().includes('https://account.mncdigital.id/login')){
    console.log("login");
    await doLogin(page,navigationPromise);
    await navigationPromise
}

// doUpload(page,navigationPromise)



// await browser.close()
})()

async function doLogin(page,navigationPromise) {
    console.log('logging in');
    await page.waitForSelector('input[id="idemail"]')
    await page.type('input[id="idemail"]', "black.alderflies@gmail.com")
    await page.type('input[type="password"]',"bemobile04")

    await page.waitForSelector('#idbtnlogin', { visible: true })
    await page.click('#idbtnlogin')

    // await navigationPromise
    // await page.waitForSelector('.nav-trigger__bar')
    // await page.click('.nav-trigger__bar')
    //
    // await page.waitForSelector('.sidebar__menu-icon--upload-video')
    // await page.click('.sidebar__menu-icon--upload-video')

}

async function loginGoogle(page,navigationPromise) {
    console.log('logging in');
    await page.waitForSelector('#google-login')
    await page.click('#google-login')

    await navigationPromise
    await page.waitForSelector('input[type="email"]')
    await page.type('input[type="email"]', "red.alderflies@gmail.com")
    await page.click('#identifierNext')

    await page.waitForSelector('input[type="password"]', { visible: true })
    await page.type('input[type="password"]',"bemobile04")

    await page.waitForSelector('#passwordNext', { visible: true })
    await page.click('#passwordNext')

    await navigationPromise
    await page.waitForSelector('.nav-trigger__bar')
    await page.click('.nav-trigger__bar')

    await page.waitForSelector('.sidebar__menu-icon--upload-video')
    await page.click('.sidebar__menu-icon--upload-video')

}

async function doUpload(page,navigationPromise) {
    const path = require('path');
    console.log("accessing upload page");
    // go to the target web
    await page.goto('https://m.vidio.com/dashboard/videos/new');

// get the selector input type=file (for upload file)
    await page.waitForSelector('input[type=file]');
    await page.waitFor(1000);

// get the ElementHandle of the selector above
    const inputUploadHandle = await page.$('.upload-video__file-form input[type=file]');

// prepare file to upload, I'm using test_to_upload.jpg file on same directory as this script
// Photo by Ave Calvar Martinez from Pexels https://www.pexels.com/photo/lighthouse-3361704/
//     let fileToUpload = 'tes.mp4';
    const fileToUpload = path.relative(process.cwd(), __dirname + '/tes.mp4');

// Sets the value of the file input to fileToUpload
    inputUploadHandle.uploadFile(fileToUpload);

//     await page.evaluate((str) => {
//         document.querySelector('textarea').value = fileToUpload;
//     document.querySelector('input[placeholder=Category]').value = "Entertainment";
// }, str);

// await page.waitForSelector('textarea');

// doing click on button to trigger upload file
//     await page.waitForSelector('#upload');
//     await page.evaluate(() => document.getElementById('upload').click());

// wait for selector that contains the uploaded file URL
//     await page.waitForSelector('#upload-link');
//     await page.waitFor(5000);

// get the download URL
//     let downloadUrl = await page.evaluate(() => {
//         return document.getElementById('upload-link').value;
// });
}