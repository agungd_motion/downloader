<?php
function getseriesList($url)
{
    global $bkurl;
    echo "GETSERIESPAGE" . PHP_EOL;
    echo "======================" . PHP_EOL;
    $startpage = 1;
    $endpage = 1;
    $timeout = 0;
    $data = array();
    for ($x = $startpage; $x <= $endpage; $x++) {
        $toinsert = array();
        $paginationpage = $url;
        if ($x > 1) $paginationpage .= "page/" . $x . "/";
        echo "PAGE $x" . PHP_EOL;

        $html = file_get_html($paginationpage, $timeout);
        if ($html == false) {
            $html = file_get_html($paginationpage, $timeout);
        }
        if ($html == false) {
            $html = file_get_html($paginationpage, $timeout);
        }
        if ($html == false) {
            $html = file_get_html($paginationpage, $timeout);
        }
        if ($html == false) {
            return $toinsert;
        }
        $cou = 1;
        foreach ($html->find('div.moviefilm') as $mf) {
            $rawtitle = "";
            $moviepage = "";
            $season = array("1");
            foreach ($mf->find('div.movief a') as $e) {
                $rawtitle = $e->innertext;
                $moviepage = $e->href;
            }

            echo $rawtitle . PHP_EOL;
            echo $moviepage . PHP_EOL;
            $normtitle = normalizeTitle($rawtitle);

            $pos = strpos($normtitle, "Season") !== false ? strpos($normtitle, "Season") : 0;
            if (!empty($pos)) {
                $tempseason = trim(substr($normtitle, $pos, strlen($normtitle)));
                $tempseason = explode(" ", $tempseason);
                $cs = 0;
                foreach ($tempseason as $v) {
                    if (!is_numeric(trim($v)))
                        unset($tempseason[$cs]);
                    $cs++;
                }

                $season = $tempseason;
                $normtitle = trim(substr($normtitle, 0, $pos));
            } else {
                $tempseason = explode(" ", $normtitle);
                if (is_numeric(trim($tempseason[count($tempseason) - 1])) && strlen(trim($tempseason[count($tempseason) - 1])) < 4) {
                    $season[0] = intval($tempseason[count($tempseason) - 1]);
                    unset($tempseason[count($tempseason) - 1]);
                    $normtitle = trim(implode(" ", $tempseason));
                }
            }

            $normtitle = strtolower($normtitle);

            $temp = "";
            $type = "seri";
            $source = "Bioskop Keren";
            $row = array(
                "rawtitle" => $rawtitle,
                "moviepageurl" => $moviepage,
                "season" => $season,
                "path" => str_replace($bkurl, "", $moviepage),
            );
            $toinsert[] = $row;
            $cou++;
        }
        return $toinsert;
    }

    echo "DONE" . PHP_EOL;
    echo "======================" . PHP_EOL;
}

function getseriesepisodes($url, $season)
{
    $timeout = 0;

    $htmllink = file_get_html($url, $timeout);

    if ($htmllink == false) {
        $htmllink = file_get_html($url, $timeout);
    }
    if ($htmllink == false) {
        $htmllink = file_get_html($url, $timeout);
    }
    if ($htmllink == false) {
        $htmllink = file_get_html($url, $timeout);
    }
    $title = "";

    $result = array();
    $episodes = array();
    $count = 1;
    foreach ($season as $v) {
        $number = $v;
        $row = array();
        if ($count == 1) $row[1] = $url;
        $count++;
        foreach ($htmllink->find('div.rbgw_part a') as $e) {
            foreach ($e->find('span') as $s) {
                $numseason = $number < 10 ? "s0" . $number : "s" . $number;
                $spanname = strtolower($s->innertext);

                if (strpos($spanname, "eps") !== false) {
                    $numeps = trim(str_replace("eps", "", $spanname));
                    $numeps = explode('+', $numeps)[0];
                    $row[$numeps] = $e->href;
                } elseif (strpos($spanname, $numseason) !== false) {
                    $numeps = trim(str_replace($numseason, "", $spanname));
                    $numeps = trim(str_replace("e", "", $numeps));
                    $numeps = explode('+', $numeps)[0];
                    if (is_numeric($numeps)) $numeps = intval($numeps);
                    $row[$numeps] = $e->href;
                }
                //array_push($result[$number],$row);
            }
        }
        $result[$number] = $row;
    }

    return $result;
}

function getdetail($movieurl)
{
    $result = array();
    $itemsframe = array();
    $iframeurl = '';
    $sub = "";
    $toupdate = array();
    $htmllink = file_get_html($movieurl, 0);
    //var_dump($htmllink);
    if ($htmllink == false) {
        $htmllink = file_get_html($movieurl);
    }
    if ($htmllink == false) {
        $htmllink = file_get_html($movieurl);
    }
    if ($htmllink == false) {
        return $toupdate;
    }

    foreach ($htmllink->find('iframe') as $e) {
        $itemsframe[] = $e->src;
        $iframeurl = $e->src;
        //echo $iframeurl;
        $toupdate["iframe"][] = $iframeurl;
    }
    if (empty($director)) {
        foreach ($htmllink->find('div.filmaltiaciklama p') as $e) {
            if (strpos($e->innertext, "Director") !== false) {
                $rawdirector = $e->innertext;
                $rawdirector = explode(":", $rawdirector);
                $rawdirector = $rawdirector[1];
                $director = trim($rawdirector);
                $toupdate["director"] = $director;
                break;
            }
        }
    }

    if (empty($sub)) {
        foreach ($htmllink->find('div.filmicerik a') as $e) {
            $toupdate["subtitle"] = $e->href;
            break;
        }
    }

    if (empty($year)) {
        foreach ($htmllink->find('div.filmaltiaciklama p a') as $e) {
            if (is_numeric($e->innertext) and strlen($e->innertext) == 4) {
                $year = trim($e->innertext);
                $toupdate["year"] = $year;
                break;
            }
        }
    }

//    if(count($itemsframe)>0){
//        foreach ($itemsframe as $v){
//            $source = getSource($v);
//            if(isset($source['videourl']) and (empty($source['videourl']) or  $source['videourl'] == "https:\/\/www.youtube.co.id\/vid.mp4")){
//                continue;
//            }else{
//                break;
//            }
//        }
//        if(isset($source['videourl']) and (empty($source['videourl']) or  $source['videourl'] == "https:\/\/www.youtube.co.id\/vid.mp4")){
//            $source = getSource($iframeurl);
//        }
//    }else{
//        $source = getSource($iframeurl);
//    }
    $source = array();

    return $result = array_merge($toupdate, $source);

    //print_r($result);


}

function getIframe($iframeurl)
{
    $html2 = file_get_contents($iframeurl);
    $kode = get_string_between($html2, 'document.write( unescape( "%', '" ) );');
    if (!empty($kode)) $kode = "%" . $kode;
    $kodescript = htmlspecialchars(urldecode($kode));
    $src = get_string_between(htmlspecialchars_decode($kodescript), '<iframe src="', '"');

    return $src;
}

function getSource($iframeurl)
{
    $toupdate = array();
    $url = $iframeurl;
    if (is_array($url)) $url = $url[0];
    $html2 = getContent($url);
    $kode = "%" . get_string_between($html2, 'document.write(unescape("%', '"));');
    $dc = rawurldecode($kode);
    $packed = "eval(" . get_string_between($dc, "eval(", "{}))") . "{}))";
    $unpacker = new JavaScriptUnpacker();
    $unpack = $unpacker->unpack($packed);
    $vv = json_decode(get_string_between($unpack, "player.setup({sources:", ",advertising:"), true);
    $ss = get_string_between($unpack, 'tracks:[{file:"', '",label');
    $ret = array();
    $vv[0]['file'] = "bk.mp4";
    if (is_array($vv)) {
        if (empty($vv[0]['file']) || $vv[0]['file'] == "bk.mp4") {
            if (is_array($iframeurl)) {
                if (isset($iframeurl[1])) {

                    $html2 = getContent($iframeurl[1]);
                    $kode = "%" . get_string_between($html2, 'document.write(unescape("%', '"));');
                    $dc = rawurldecode($kode);
                    $packed = "eval(" . get_string_between($dc, "eval(", "{}))") . "{}))";
                    $unpacker = new JavaScriptUnpacker();
                    $unpack = $unpacker->unpack($packed);
                    $dlurl = get_string_between($unpack, 'window.open("', '")},');
                    if (!empty($dlurl)) {
                        $r = file_get_html($dlurl, 0);
                        if ($r !== false) {
                            $vv = array();
                            foreach ($r->find("a") as $k => $v) {
                                $vv[]['file'] = $v->href;
                            }
                        }
                    }
                }
            }
        }
    }
    $toupdate["iframeurl"] = $url;
    $toupdate["videourl"] = $vv;//htmlspecialchars_decode($decodedparsed);
    $toupdate["subtitleurl"] = $ss;
    return $toupdate;
}

function getSourceFromDL($iframeurl)
{
    $toupdate = array();
    $url = $iframeurl;
    $dlurl = "https://player.serverbk.com/dl/?url=";
    $bkid = explode("id=", $url[0])[1];
    $dlurl .= $bkid;
    $vv = array();
    if (!empty($dlurl)) {
        $r = file_get_html($dlurl, 0);
        if ($r !== false) {

            foreach ($r->find("a") as $k => $v) {
                $vv[]['file'] = $v->href;
            }
        }
    }

    $toupdate["iframeurl"] = $url;
    $toupdate["videourl"] = $vv;//htmlspecialchars_decode($decodedparsed);
    return $toupdate;
}
