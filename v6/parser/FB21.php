<?php
function getGdrivePlayerLink($g){
    $r= "";
    try{
$s = "/usr/bin/node /home/motion/dw/htmldom/pup.js '$g'";
    $r = shell_exec($s);
}catch(Exception $e){}
    return $r;
}

function getEpisodeFB21($movieurl,$season){
    $result = array();
    $itemsframe = array();
    $iframeurl = '';
    $selectedSeason = $season;
    if(strpos($movieurl, 'episode-')!==false){
        $del = get_string_between($movieurl,"episode-","-2019");
        $movieurl = str_replace('episode-'.$del, '', $movieurl);
    }
    $htmllink = file_get_html($movieurl,0);
    $toupdate = array();
    $episodes = array();
    if($htmllink===false){
        return $toupdate;
    }

    foreach ($htmllink->find("div#content a.btn-primary") as $k => $e){
        $row = array();
        $rawlabel = trim($e->innertext);
        if(strpos($rawlabel, 'Info')!== false){
            continue;
        }
        if(strpos($rawlabel, "S")!==false){
            $season = get_string_between($rawlabel,"S","E");
            $label = substr($rawlabel,strpos($rawlabel, "E")+1);
        }
        $label = trim(str_replace("E","",strtolower($rawlabel)));
        $numeps = trim(str_replace("end","",strtolower($label)));

        $numeps = explode("-",$numeps)[0];
        $numeps = explode("*",$numeps)[0];
        $numeps = explode(" ",$numeps)[0];

        if(strpos($label,".")!==false){
            $numeps = $k+1;
        }
        if(!is_numeric($numeps)){
            $numeps = $k+1;
        }
        $numeps = intval($numeps);
//        $numeps = $k+1;
        // if($k==0){
        //     $epsurl = $movieurl;
        // }else{
        $epsurl = $e->href;
        // }
        // echo $epsurl;
        // if()

        $row = array(
            "label" => $rawlabel,
            "numeps" => $numeps,
            "url" => $epsurl,
            "season" => $season
        );
        if($selectedSeason==$season)
        array_push($episodes,$row);

    }
    $toupdate['episodes'] = $episodes;

    $source=array();

    return $result = array_merge($toupdate,$source);
}

function getSourceFB21($dlurl){
    $toupdate = array();

    $htmllink = file_get_html($dlurl);

    $vv = array();
    $table = $htmllink->find('table',0);
    $iframeurl = '';
    $tr = array();

    foreach($htmllink->find('div#content') as $e) {
        foreach ($e->find('h3 a.btn-primary') as $v) {
            $iframeurl = $v->href;
        }
    }
    $dlink = file_get_html($iframeurl);
    foreach($dlink->find('table') as $e) {
        foreach ($e->find('tr td a') as $v) {
            if (strpos($v->href, 'google') !== false or strpos($v->href, 'ns21.online') !== false) {
                $url = $v->href;
                if(strpos($url, "amp;")!==false){
                    $url = str_replace("amp;", '', $url);
                }
                $item = array(
                    'file' => $url,
                    'label' => $v->innertext,
                    'type' => 'Video/mp4'
                );
                array_push($vv, $item);
            }
        }
    }

    if(count($vv)==1 && strpos($vv[0]['file'],"ns21.online")!==false) {
        $v = $vv[0]['file'];
        $s = explode("/", explode("#", $v)[0]);
        $tmp = $s[count($s) - 1];
        $loader = "https://ns21.online/api/source/" . $tmp;
        // echo $loader;die();
        $param = "r=&d=t";
        $r = getContent($loader, "POST");
        $c = json_decode($r);

        $tmpvv = $c->data;
        foreach ($tmpvv as $k=>$v){
            array_push($vv, $v);
        }
    }

    if(count($vv)==0){
        $gdriveiframe = "";
        foreach($dlink->find('iframe') as $e) {
            $gdriveiframe = $e->src;
            if(strpos($gdriveiframe,"gdriveplayer")!==false) break;
        }
        if(!empty($gdriveiframe)){
            if($gdriveiframe[0]=='/')$gdriveiframe = "https:".$gdriveiframe;
            $gdriveiframe = str_replace("download.php","embed2.php",$gdriveiframe);

            $url = trim(getGdrivePlayerLink($gdriveiframe));
            $item = array(
                'file' => $url,
                'label' => "ori",
                'type' => 'Video/mp4'
            );
            array_push($vv, $item);
        }
    }
    // $toupdate["iframeurl"]=$dlurl;
    $toupdate["videourl"] = $vv;
    //htmlspecialchars_decode($decodedparsed);
    $toupdate["subtitleurl"] = "";
    return $toupdate;
}
