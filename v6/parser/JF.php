<?php

function getEpisodeJF($movieurl,$season){
    $result = array();
    $itemsframe = array();
    $iframeurl = '';
    $htmllink = file_get_html($movieurl,0);
    //var_dump($htmllink);
    $toupdate = array();
    $episodes = array();
    if($htmllink===false){
        return $toupdate;
    }

    foreach ($htmllink->find("a[itemprop=relatedLink/pagination]") as $k => $e){
        $row = array();
        $rawlabel = trim($e->innertext);
        $label = str_replace("eps","",strtolower($rawlabel));
        $numeps = $k+1;
        $epsurl = $e->href;


        $row = array(
            "label" => $rawlabel,
            "numeps" => $numeps,
            "url" => $epsurl,
            "season" => $season
        );
        array_push($episodes,$row);
    }
    if($season>0 and count($episodes)==0){
        $row = array(
            "label" => "1",
            "numeps" => 1,
            "url" => $movieurl,
            "season" => $season
        );
        array_push($episodes,$row);
    }
    $toupdate['episodes'] = $episodes;

    $source=array();

    return $result = array_merge($toupdate,$source);
}

function getdetailJF($movieurl){
    $result = array();
    $itemsframe = array();
    $iframeurl = '';
    $htmllink = file_get_html($movieurl,0);
    $found = 0;
    $toupdate =array();
    if($found==0){
        foreach($htmllink->find('.blink-btn') as $e) {
            $iframeurl = $e->href;
            $toupdate["iframe"] = $iframeurl;
            $found=1;
            break;
        }
    }
    if($found==0){
        foreach($htmllink->find('.muvipro-player-tabs .pull-left a') as $e) {
            $iframeurl = $e->href;
            $toupdate["iframe"] = $iframeurl;
            $found=1;
            break;
        }
    }

    if($found==0){
        foreach($htmllink->find('.gmr-player-nav .pull-right a') as $e) {
            $iframeurl = $e->href;
            $toupdate["iframe"] = $iframeurl;
            $found=1;
            break;
        }
    }
    $source=array();
    return $result = array_merge($toupdate,$source);
}

function getSourceJF($dlurl){
    $toupdate = array();
    $htmllink = file_get_html($dlurl);
    $vv = array();
    $table = $htmllink->find('table',0);
    foreach($table->find('tr') as $e) {
        $td = $e->find('td');
        if (count($td) >= 3) {
            $label = $td[0]->find("strong",0)->innertext;
            $url = $td[1]->find("a",0);

            if ($url !== false) {
                if(isset($url->href)){
                    $url = $url->href;
                    $size = getRemoteFilesize($url,false);
                    if($size>0 || strpos($url,"lkc21.net")!==false){
                        $item = array(
                            "file" => $url,
                            "label" => $label,
                            "type" => "video/mp4"
                        );
                        array_push($vv, $item);
                    }
                }
            }
        }
    }

    if(count($vv)==1 && strpos($vv[0]['file'],"lkc21.net")!==false) {
        $v = $vv[0]['file'];
        $vv = getFembed($v);
    }
    $toupdate["iframeurl"]=$dlurl;
    $toupdate["videourl"] = $vv;//htmlspecialchars_decode($decodedparsed);
    $toupdate["subtitleurl"] = "";
    return $toupdate;
}

function getFembed($fembedurl){
    $v = $fembedurl;
    $vv = array();
    $s = explode("/", explode("#", $v)[0]);
    $purl = parse_url($fembedurl);
    $domain = $purl['host'];

    $tmp = $s[count($s) - 1];
    $loader = "https://$domain/api/source/" . $tmp;
    $param = "r=&d=t";
    $r = getContent($loader, "POST");
    $c = json_decode($r,true);

    $tmpvv = $c['data'];
    foreach ($tmpvv as $k=>$v){
        array_push($vv, $v);
    }
    return $vv;
}
