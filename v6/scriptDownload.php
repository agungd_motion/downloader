<?php
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
require_once('const.php');
require_once('func.php');
require_once('htmldom/simple_html_dom.php');
require_once('telegram-bot/sendmessage.php');
require_once('parser/JF.php');
require_once('parser/FB21.php');
require_once('parser/BK.php');
require_once('parser/movie21.php');
require_once('parser/DQ.php');

require_once('plugin/myjdapi.php');
require_once('plugin/unpacker.php');
//goto getqueue;
$maxlink = isset($argv[1]) ? $argv[1] : 1;
$countlink = 0;


if (date('H') > 19) {
    $t = testSpeed();
    if (!empty($t)) {
        $s = explode(" ", $t);
        if (count($s) > 1) {
            if ($s[1] == "Mb") {
                $sp = floatval(trim($s[0]));
                if ($sp > 4) {
                    $maxlink = 5;
                } elseif ($sp > 3) {
                    $maxlink = 3;
                } elseif ($sp > 2) {
                    $maxlink = 2;
                }

            }
        }

    }
}
echo "DOWNLOAD DIR :" . DL_DIR . PHP_EOL;
if (!is_dir(DL_DIR)) die("hdd notfound");
$base = DL_DIR . "/dd";
$basedone = MAIN_DIR;
//goto drakorbaru;
print_r("connecting...." . PHP_EOL);
$my = new MYJDAPI(JD_USER, JD_PASS, JD_NAME);
$links = $my->queryLinks();
// var_dump($links);
if ($links == '') {
    echo "try again\n";
    sleep(60);
    $links = $my->queryLinks();
}

if ($links == '') die("connecting failed");
// Check if still downloading
$links = json_decode($links, true);
//print_r($links);die();
if (isset($links['data'])) {
    foreach ($links['data'] as $k => $v) {
        $status = isset($v['status']) ? $v['status'] : '';
        $perc = round($v['bytesLoaded'] / $v['bytesTotal'] * 100, 2);
        if (empty($status) and $perc < 100) die("there are still unfinished download");
        if ($status == 'Download') {
            //if download 75% or ETA 15 min getnextlink
            if ($k == count($links['data']) - 1 and ($perc > 75 || $v['eta'] <= 900)) {
                echo $v['name'] . " still downloading ({$perc}%)" . PHP_EOL;
                //$maxlink = 1;
                echo "getNextLink:" . PHP_EOL;
            } else {
                die($v['name'] . " still downloading ({$perc}%)");
            }

        }
    }
}

//GETQUEUE FIRST
getqueue:
$q = getQueue();
foreach ($q as $key => $v) {
    var_dump($v);
    $dirtype = "movies";
    $package = str_replace(" ", "-", trim(implode(" ", explode("/", $v['path']))));
    $infid = $v['inflixerid'];
    $inftitle = $v['title'];
    $link = $v['link'];
    $plink = explode("?sub=", $link);
    $link = $plink[0];
    $s = $v['season'];
    $e = $v['episode'];
    $id = $v['id'];
    $status = $v['status'];
    $date = $v['release_date'];
    $year = substr($date, 0, 4);
    $sumber = $v['sumber'];
    $sumber = explode(".", $sumber);
    array_pop($sumber);
    if(strpos($sumber[0],"www")!==false){
        unset($sumber[0]);
    }
    $sumber = implode(".", $sumber);

    $parseurl = parse_url($link);

    echo $sumber . PHP_EOL;
    if ($status == "R") continue;
    if ($s > 0) {
        $dirtype = "series";
        if (empty($e)) {
            switch ($sumber) {
                case "bioskopkeren":
                    $eps = getseriesepisodes($link, array($s));
                    print_r($eps);
                    foreach ($eps[$s] as $kep => $vep) {
                        $e = $kep;
                        $link = $vep;
                        $add = addQueue($link, $s, $e, $sumber, $infid, $inftitle, $date);
                        print_r($add);

                    }
                    delQueue($id);
                    break;
                case "juraganfilm":
                    $eps = getEpisodeJF($link, $s);
                    print_r($eps);
                    foreach ($eps['episodes'] as $kep => $vep) {
                        $e = $vep['numeps'];
                        $link = $vep['url'];
                        $add = addQueue($link, $s, $e, $sumber, $infid, $inftitle, $date);
                        print_r($add);
                    }
                    delQueue($id);
                    break;
                case "filmbioskop":
                    $eps = getEpisodeFB21($link, $s);
                    print_r($eps);
                    foreach ($eps['episodes'] as $kep => $vep) {
                        $e = $vep['numeps'];
                        $link = $vep['url'];
                        $v['path'] = parse_url($link)['path'];
                        $add = addQueue($link, $s, $e, $sumber, $infid, $inftitle, $date);
                        print_r($add);
                    }
                    delQueue($id);
                    break;
                case "dramaqu":

                    break;
            }
        }

        $tmppackage = explode("/", $v['path']);
        if ($sumber == "bioskopkeren") {
            unset($tmppackage[2]);
        } elseif ($sumber == "juraganfilm") {
            unset($tmppackage[3]);
        }
        $package = str_replace(" ", "-", trim(implode(" ", $tmppackage)));
        $package = str_replace(".", "-", $package);
        $package = str_replace("&", "-", $package);
        $package = str_replace("=", "-", $package);
//        $package .= "-" . $s . "-" . $e;
    }

    // if($status=="Q"){
    echo $package . " | " . $status . PHP_EOL;
    $needsubtitle = array("bioskopkeren", "dramaqu");
    if ((in_array($sumber, $needsubtitle) && !isSrtExist($base . DIRECTORY_SEPARATOR . $package) ||
            !isMp4Exist($base . DIRECTORY_SEPARATOR . $package)) && $status == "Q"
    ) {
        $detail = array();
        switch ($sumber) {
            case 'files':
            case 'zippyshare':
            case 'racaty':
            case 'gdrive':
            case 'hxfile':
            case 'uptobox':
            case 'drive.google':
                $res = $my->addLinks($link, $package);
                break;
            default:
                break;
        }
    } else {
        if (is_numeric($infid)) {
            $newname = "{$infid}_s{$s}e{$e}";
            $title = !empty($inftitle) ? normalizeTitle($inftitle) : normalizeTitle(str_replace("-", " ", $package));
            $infdirname = $infid . "-" . str_replace(" ", "-", strtolower($title));
            $newpath = $dirtype . DIRECTORY_SEPARATOR . $year . DIRECTORY_SEPARATOR . $infdirname . DIRECTORY_SEPARATOR;
            if ($dirtype == 'series') {
                $newpath .= "s" . $s . DIRECTORY_SEPARATOR . "e" . $e . DIRECTORY_SEPARATOR;
            }

            $m = moveFile($base . DIRECTORY_SEPARATOR . $package, $basedone . DIRECTORY_SEPARATOR . $newpath, $newname);
            if ($m) updateQueue($id, "D");
            // die();
        }
    }
    // }
    if ($countlink >= $maxlink) break;

}
