<?php
function getQueue()
{
    $code = 50;
    $post = array(
        "code" => $code
    );
    $res = getContent(ROBO_API_URL, "POST", $post);
    $rd = json_decode($res, true);
    return $rd;
}

function updateQueue($id, $status)
{
    $code = 51;
    $post = array(
        "code" => $code,
        "id" => $id,
        "status" => $status
    );
    $res = getContent(ROBO_API_URL, "POST", $post);
    $rd = json_decode($res, true);
    return $rd;
}

function delQueue($id)
{
    $code = 53;
    $post = array(
        "code" => $code,
        "id" => $id
    );
    $res = getContent(ROBO_API_URL, "POST", $post);
    $rd = json_decode($res, true);
    return $rd;
}

function isSrtExist($dir)
{
    if (!is_dir($dir)) return false;
    $dirs = scandir($dir);

    foreach ($dirs as $k => $v) {
        if ($v == "." || $v == "..") continue;
        if (strpos($v, ".srt") !== false) return true;
    }
    return false;
}

function isMp4Exist($dir)
{
    global $bp;
    echo str_replace($bp, "", $dir) . PHP_EOL;
    if (!is_dir($dir)) return false;
    $dirs = scandir($dir);
    // print_r($dirs);
    foreach ($dirs as $k => $v) {
        if ($v == "." || $v == "..") continue;

        if (strpos($v, ".mp4") !== false) return true;
    }
    return false;
}

function moveFile($olddir, $newdir, $newname)
{
    if (!is_dir($newdir)) {
        mkdir($newdir, 0777, true);
    }
    if (!is_dir($olddir)) {
        die("error");
    }
    $dirs = scandir($olddir);
    // print_r($dirs);
    $cm = 1;
    $ret = false;
    foreach ($dirs as $k => $v) {
        if ($v == "." || $v == ".." || $v == "tmp.mp4" || $v == "tmp.srt") continue;
        if (strpos($v, ".part") !== false) {
            continue;
        } elseif (strpos($v, ".mp4") !== false) {
            echo "moving file to $newdir" . PHP_EOL;
            $res = rename($olddir . DIRECTORY_SEPARATOR . $v, $newdir . DIRECTORY_SEPARATOR . $newname . ".mp4");
            if ($res) {
                file_put_contents($olddir . DIRECTORY_SEPARATOR . "tmp.mp4", "");
                if ($cm == 1) sendmessage($newdir . " done");
                echo "moving done" . PHP_EOL;
                $ret = true;
                // syncStorage();
            }
            $cm++;
        } elseif (strpos($v, ".srt") !== false) {
            $res = rename($olddir . DIRECTORY_SEPARATOR . $v, $newdir . DIRECTORY_SEPARATOR . $newname . ".srt");
            if (strpos($v, "eng") === false
                && strpos($v, "belumada") === false
                && strpos($v, "unknown") === false
            ) {
                if ($res) file_put_contents($olddir . DIRECTORY_SEPARATOR . "tmp.srt", "");
            }
        }
    }
    return $ret;
}

function uploadToAWS($dir, $awsdir, $recursive = false)
{
    echo "file notfound in server, Uploading..." . PHP_EOL;
    $cp = "/usr/local/bin/aws s3 cp $dir s3://$awsdir --acl public-read";
    if ($recursive) {
        $cp .= " --recursive";
    }
    // echo $cp.PHP_EOL;die();
    $scp = shell_exec($cp);
    return $scp;
}

function setVideoAddLog($log = null)
{
    if (is_array($log)) {
        file_put_contents(VIDEOADDFILE, implode("\n", $log));
    }
}

function setUploadedLog($log = null)
{
    if (is_array($log)) {
        file_put_contents(UPLOADEDFILE, implode("\n", $log));
    }
}

function getVideoAddLog()
{
    if (!is_file(VIDEOADDFILE)) {
        file_put_contents(VIDEOADDFILE, "");
    }
    $videoaddlog = file_get_contents(VIDEOADDFILE);
    return $videoaddlog = explode("\n", $videoaddlog);
}

function getUploadedLog()
{
    if (!is_file(UPLOADEDFILE)) {
        file_put_contents(UPLOADEDFILE, "");
    }
    $log = file_get_contents(UPLOADEDFILE);
    return $log = explode("\n", $log);
}

function better_scandir($dir, $sorting_order = SCANDIR_SORT_ASCENDING)
{

    /****************************************************************************/
    // Roll through the scandir values.
    $files = array();
    foreach (scandir($dir, $sorting_order) as $file) {
        if ($file[0] === '.') {
            continue;
        }
        $files[$file] = filemtime($dir . '/' . $file);
    } // foreach

    /****************************************************************************/
    // Sort the files array.
    if ($sorting_order == SCANDIR_SORT_ASCENDING) {
        asort($files, SORT_NUMERIC);
    } else {
        arsort($files, SORT_NUMERIC);
    }

    /****************************************************************************/
    // Set the final return value.
    $ret = array_keys($files);

    /****************************************************************************/
    // Return the final value.
    return ($ret) ? $ret : false;

} // better_scandir

function videoAdd($payloadVideo)
{
    $post = array(
        "total_videos" => count($payloadVideo),
        "videos" => $payloadVideo
    );
    print_r($post);
    $url = INF_VIDEO_ADD_URL;
    var_dump($url);
    $insert = getContent($url, "POST", $post, array(
        'Content-Type: application/json'
    ));

    return $insert;
}

function get_string_between($string, $start, $end)
{
    $string = ' ' . $string;
    $ini = strpos($string, $start);
    if ($ini == 0) return '';
    $ini += strlen($start);
    $len = strpos($string, $end, $ini) - $ini;
    return substr($string, $ini, $len);
}

function getContent($url, $type = null, $post = null, $header = null)
{
    $ch = curl_init();
    $timeout = 5;
    curl_setopt($ch, CURLOPT_URL, $url);

    if (!empty($type)) {
        if ($type == "POST" || $type == "GET") {
            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, $type);
        }
    }

    if (!empty($post)) {
        $data = $post;
        if (is_array($post)) {
            $data = json_encode($post);
        }
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
    }
    curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, $timeout);
//    curl_setopt($ch, CURLOPT_HTTPHEADER, array(
//        'Content-Type: application/json'
//    ));
    if (is_array($header)) {
        curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
    }

    $html = curl_exec($ch);
    curl_close($ch);
    return $html;
}

function replaceAccents($str)
{
    $search = explode(",",
        "ć,ç,æ,œ,á,é,í,ó,ú,à,è,ì,ò,ù,ä,ë,ï,ö,ü,ÿ,â,ê,î,ô,û,å,ø,Ø,Å,Á,À,Â,Ä,È,É,Ê,Ë,Í,Î,Ï,Ì,Ò,Ó,Ô,Ö,Ú,Ù,Û,Ü,Ÿ,Ç,Æ,Œ");
    $replace = explode(",",
        "c,c,ae,oe,a,e,i,o,u,a,e,i,o,u,a,e,i,o,u,y,a,e,i,o,u,a,o,O,A,A,A,A,A,E,E,E,E,I,I,I,I,O,O,O,O,U,U,U,U,Y,C,AE,OE");
    return str_replace($search, $replace, $str);
}

function normalizeTitle($title)
{
    $title .= " ";
    $newtitle = strtolower($title);
    $newtitle = urldecode($newtitle);
    $newtitle = replaceAccents($newtitle);
    $toremove = array(
        " sub indo ",
        "subtitle indonesiaa",
        "subtitle indonesia",
        "subitle indonesia",
        "indonesia sub",
        "nonton dan download",
        "indonesia subtitle",
        "short video",
        "nonton film online",
        "nonton film streaming",
        "nonton streaming",
        "nonton steaming",
        "bioskop keren",
        "nonton drama korea",
        "nonton drama",
        "nonton serial",
        "film seri",
        "di bioskopkeren",
        "bioskopkeren",
        "nonton drakor",
        "nonton movie",
        "nonton bioskop",
        "(ongoing)",
        "(tamat)",
        "nonton film",
        "nonon film",
        "nonton online",
        "nonton",
        "drama korea",
        "chinese movie",
        "india movie",
        "movie india",
        "movie korea",
        "thailand movie",
        "japan movie",
        "korea movie",
        "horror thailand",
        "korean movie",
        "film korea",
        "'",
        "’",
        "anime",
        "jf",
        "gt",
        "&#8217;",
        "&#8217"
    );

    $toremovewithspace = array(
        ":", ";", "?", "!", ".", ",", "-", "–", "&#8211"
    );

    $newtitle = str_replace("&#038;", "and", $newtitle);

    foreach ($toremove as $key => $value) {
        $newtitle = str_replace($value, "", $newtitle);
    }
    foreach ($toremovewithspace as $key => $value) {
        $newtitle = str_replace($value, " ", $newtitle);
    }

    if (substr(trim($newtitle), 0, 3) != "cam") {
        $newtitle = str_replace(" cam ", "", $newtitle);
    }

    $toremoveyear = array();
    for ($i = 1940; $i <= date("Y"); $i++) {
        $toremoveyear[] = $i;
    }

    foreach ($toremoveyear as $key => $value) {
        if (substr(trim($newtitle), 0, 4) != $value) {
            $newtitle = str_replace($value, "", $newtitle);
        }
    }

    //$pos = strpos($newtitle,"season") > 0 ? strpos($newtitle,"season") : strlen($newtitle);
    //$newtitle = substr($newtitle, 0,$pos);

    $pos = strpos($newtitle, "subtitle") > 0 ? strpos($newtitle, "subtitle") : strlen($newtitle);
    $newtitle = substr($newtitle, 0, $pos);

    $newtitle = str_replace("&", "and", $newtitle);
    $newtitle = str_replace("()", "", $newtitle);

    $newtitle = removeWhitespace($newtitle);

    return trim(ucwords($newtitle));
}

function removeWhitespace($newtitle)
{
    return $newtitle = preg_replace(array('/\s{2,}/', '/[\t\n]/'), ' ', $newtitle);
}

function syncStorage()
{
    $script = "ps -aux | grep aws";
    $script2 = "/usr/local/bin/aws s3 sync /mnt/usb-Seagate_BUP_Portable_00000000NAB90HTN-0\:0-part1/Downloads/videos s3://myd1/videos --acl public-read > /dev/null 2>&1 &";
    $s = shell_exec($script);
    print_r($s);
    $ps = "/usr/local/bin/aws s3 sync /mnt/usb-Seagate_BUP";
    if (strpos($s, $ps) === false) {
        echo "sync is running";
        echo $script2;
        $sync = exec($script2);
//        print_r($sync);
    } else {
        echo "sync already running";
    }
}

function addQueue($link, $s, $e, $sumber, $infid, $title, $date, $path = null, $playertype)
{
    $code = 52;
    $param = array(
        "link" => $link,
        "season" => $s,
        "episode" => $e,
        "sumber" => $sumber,
        "inflixerid" => $infid,
        "inflixertitle" => $title,
        "release_date" => $date,
        "player_type" => $playertype,
    );
    if (!empty($path)) $param['path'] = $path;
    $post = array(
        "code" => $code,
        "data" => $param
    );
    $res = getContent(ROBO_API_URL, "POST", $post);
    print_r($res);
    $rd = json_decode($res, true);
    return $rd;
}

function getConf()
{
    $url = "https://robo.inflixer.com/v4/api/Endpoint.php";
    $post = array(
        "code" => 100,
    );
    $res = getContent($url, "POST", $post);
    $ret = json_decode($res, true);
    return $ret;
}

function getBKbaseURL($config = null)
{
    if (!is_array($config)) {
        $config = getConf();
    }
    $base = "https://bioskopkeren.kim/";
    if (is_array($config)) {
        if (isset($config['bk_baseurl']) and !empty($config['bk_baseurl'])) {
            $base = $config['bk_baseurl'];
        }
    }
    return $base;
}

function getInfID($path)
{
    $url = "https://robo.inflixer.com/v4/api/Endpoint.php";
    $code = 9;
    $post = array(
        "code" => $code,
        "paths" => array($path)
    );
    $res = getContent($url, "POST", $post);
    $rd = json_decode($res, true);
    return $rd[0];
}

function getCurrentSession()
{
    if (!is_file(dirname(__FILE__) . "/sess")) {
        file_put_contents(dirname(__FILE__) . "/sess", "init");
    }
    $sess = file_get_contents(dirname(__FILE__) . "/sess");
    if (!checkAccount($sess)) {
        echo "new";
        $sess = getNewSession();
    }
    return trim($sess);
}

function checkAccount($sess)
{
    $acc = getContent(trim(INF_ACCOUNT . $sess));
    $obj = json_decode($acc, true);
//    print_r($obj);
    if (isset($obj['status_message']) and $obj['status_message'] == "Session denied.") {
        return false;
    }
    return true;
}

function getNewSession()
{
    $res = getContent(INF_REQUEST_TOKEN);
    $rd = json_decode($res, true);
    $token = $rd['request_token'];
    $post = array(
        "email" => RAJA_USER,
        "id" => "eytriqw3476iw374td",
        "last_name" => "downloader",
        "request_token" => $token,
        "first_name" => "downloader",
        "avatar" => "https://lh3.googleusercontent.com/a-/AAuE7mDUX_1ePFt8UbyPbPjg8oxpB2FoV4FXXhwk7Qq5Qw"
    );

    $res = getContent(INF_LOGIN_GOOGLE, "POST", $post);
    $rd = json_decode($res, true);

    if (isset($rd['success']) and $rd['success']) {
        $sessionid = $rd['session_id'];
        $p = file_put_contents(dirname(__FILE__) . "/sess", $sessionid);
        var_dump($p);
        return $sessionid;
    }

    return false;
}

function testSpeed()
{
    echo "testing Speed...." . PHP_EOL;
    $r = shell_exec('speedtest-cli');
    $string = ' ' . $r;
    $ini = strpos($string, "Download:");
    if ($ini == 0) return '';
    $ini += strlen("Download:");
    $len = strpos($string, "it/s", $ini) - $ini;
    $res = trim(substr($string, $ini, $len));
    echo $res . PHP_EOL;
    return $res;
}

function getRemoteFilesize($url, $formatSize = true, $useHead = true)
{
    if (false !== $useHead) {
        stream_context_set_default(array('http' => array('method' => 'HEAD')));
    }
    $head = array_change_key_case(get_headers($url, 1));
    // content-length of download (in bytes), read from Content-Length: field
    $clen = isset($head['content-length']) ? $head['content-length'] : 0;
    if (isset($head['content-type'])) {
        if ($head['content-type'] != "video/mp4") return 0;
    }

    // cannot retrieve file size, return "-1"
    if (!$clen) {
        return -1;
    }

    if (!$formatSize) {
        return $clen; // return size in bytes
    }

    $size = $clen;
    switch ($clen) {
        case $clen < 1024:
            $size = $clen . ' B';
            break;
        case $clen < 1048576:
            $size = round($clen / 1024, 2) . ' KiB';
            break;
        case $clen < 1073741824:
            $size = round($clen / 1048576, 2) . ' MiB';
            break;
        case $clen < 1099511627776:
            $size = round($clen / 1073741824, 2) . ' GiB';
            break;
    }

    return $size; // return formatted size
}