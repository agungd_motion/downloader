<?php //sss
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
require_once('const.php');
require_once('telegram-bot/sendmessage.php');

if(!is_dir(MAIN_DIR)){
    die("dir not found");
}

$videoaddlog = getVideoAddLog();
$uploadedlog = getUploadedLog();
$uploading = false;
$uploadedNote = "uploaded:\n";
$notfound = 0;
//print_r($videoaddlog);
//die();
//ob_start();
echo "cronstart:";
echo date("Y-m-d H:i:s").PHP_EOL;
echo "<pre>";
_scandir(MAIN_DIR);
if(strlen($uploadedNote)>15)sendMessage($uploadedNote);
echo PHP_EOL."cronend:".date("Y-m-d H:i:s").PHP_EOL;

//$htmlStr = ob_get_contents();
//ob_end_clean();
//$filename="up.txt";
//file_put_contents($filename, $htmlStr);
die();
function _scandir($dir){
    global $videoaddlog;
    global $uploadedNote;
    global $uploadedlog;
    global $notfound;
    global $uploading;
    $sd = scandir($dir);
    foreach ($sd as $k => $v){
        if($v=="." || $v=="..") continue;
//        echo count($sd)."/$k".PHP_EOL;
        if(is_dir($dir.$v)){
            _scandir($dir.$v.DIRECTORY_SEPARATOR);
        }elseif(is_file($dir.$v)){

            $p = dirname($dir.$v);
            $type = explode(DIRECTORY_SEPARATOR,$p);          
            if(count($type)<6)continue;
            
            $type=$type[5];
            $filename = $v;
            $fullpath = $p.DIRECTORY_SEPARATOR.$filename;

            if(!in_array($p,$videoaddlog)){
                echo $dir.$v.PHP_EOL;
            
                if(strpos($filename,"mp4")!==false){
                    $filenameSubtitle = str_replace(".mp4",".srt", $filename);
                    $fullpathSubtitle = $p.DIRECTORY_SEPARATOR.$filenameSubtitle;
                    $sub = "";
                    if(is_file($fullpathSubtitle)){
                        if(!in_array($fullpathSubtitle, $uploadedlog)){
                            $awsdir = AWS_SUB_DIR;
                            $awsls = '/usr/local/bin/aws s3 ls '.$awsdir;
                            echo $awsls.PHP_EOL;
                            $se = shell_exec($awsls);
                            echo $se.PHP_EOL;
                            if(empty($se) || strpos($se,$filenameSubtitle)===false){
            //                    echo $p;die();
                                uploadToAWS($fullpathSubtitle,$awsdir.$filenameSubtitle);
                                $se = shell_exec($awsls);
                                echo $se.PHP_EOL;
                            }
                            
                            if(strpos($se,$filenameSubtitle)!==false){
                                $sub = AWS_SUB_URL.$filenameSubtitle;
                                $uploadedlog[] = $fullpathSubtitle;
                            }    
                        }else{
                            $sub = AWS_SUB_URL.$filenameSubtitle;
                        }
                        
                    }
                    if(!existsInMetube($filename)){
                        if(!$uploading){
                            uploadToMetube($fullpath);
                            //log
                            setVideoAddLog($videoaddlog);
                            setUploadedLog($uploadedlog);
                            $uploading = true;    
                        }else{
                            continue;
                        }
                    }else{
                        $vid = getMetubeData($filename);
                        if($vid!==false){
                            if(!empty($vid)){
                                $uploadedlog[] = $fullpath;
                                $s = null;
                                $e = null;
                                $exvid = explode("_",$filename);
                                $inflixerid = $exvid[0];
                                if($type == "series"){
                                    $raws = $exvid[1];
                                    $s = get_string_between($raws,"s","e");
                                    $e = get_string_between($raws,"e",".");
                                    if($e==''){
                                        $e = substr($raws,strpos($raws,"e")+1);
                                    }
                                }
                                if(!empty($sub)){
                                    $vid .= "?sub=".$sub;
                                }
                                if(!is_numeric($inflixerid)){
                                    echo "inflixerid is not numeric, skipped".PHP_EOL;
                                    continue;
                                }
                                $payload = array(
                                    "label"    => "Rose Box",
                                    "url"    => $vid,
                                    "subtitle"  => null,
                                    "season"  => $s,
                                    "episode"  => $e,
                                    "type"    => "external",
                                    "titles_id"=>$inflixerid
                                );
                                $videoadd = videoAdd(array($payload));
                                $rd = json_decode($videoadd);
                                if($rd->success){
                                    if($rd->total_videos==1){
                                        echo "videoadd success".PHP_EOL;
                                        $uploadedNote.=str_replace(MAIN_DIR,"",$dir.$v)."\n";
                                        $videoaddlog[] = $p;
                                        setVideoAddLog($videoaddlog);
                                        setUploadedLog($uploadedlog);
                                    }else{
                                        echo "videoadd fail $videoadd".PHP_EOL;
                                    }
                                }
                            }
                        }
                    }
                }
                break;
            }
        }
        if($notfound>100)break;
    }
    if(strlen($uploadedNote)>1500){
        sendMessage($uploadedNote);
        $uploadedNote = "uploaded:\n";
    }
    setVideoAddLog($videoaddlog);
    setUploadedLog($uploadedlog);
}

